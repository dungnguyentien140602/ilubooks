

const initialState = {
    user: null,
    isAuthenticated: false,
    error: null,
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REGISTER_SUCCESS':
            return {
                ...state,
                user: action.payload,
                isAuthenticated: true,
            };
        case 'Update_User':
            return {
                ...state,
                user:{
                    ...state.user,
                    cart:action.payload
                } 
            }
        case 'REGISTER_FAIL':
            return {
                ...state,
                user: null,
                isAuthenticated: false,
                error: action.payload,
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                user: action.payload,
                isAuthenticated: true,
                error: null,
            };
        case 'LOG_OUT':
            localStorage.removeItem('token')
            return {
                ...state,
                isAuthenticated: false,
                user: null,
            
            }
        case 'LOGIN_FAIL':
        default:
            return state;
    }
};

export default userReducer;