import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import userReducer from './userReducers'
import persistStore from 'redux-persist/es/persistStore'
const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  auth: userReducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
  reducer: persistedReducer,
})

export const persistor = persistStore(store)

export default {store, persistor}
