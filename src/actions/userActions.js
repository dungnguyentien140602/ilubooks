import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const registerUser =
  (firstname, lastname, email, mobile, password) => (dispatch) => {
    axios
      .post("http://localhost:5000/api/user/register", {
        firstname: firstname,
        lastname: lastname,
        email: email,
        mobile: mobile,
        password: password,
      })
      .then((res) => {
        if (res.data.status === "success") {
          localStorage.setItem("user", JSON.stringify(res.data));
          dispatch({
            type: "REGISTER_SUCCESS",
            payload: res.data,
          });
          window.location.href = "/login";
        }
        if (res.data.status === false) {
          toast.error(res.data.msg, {
            autoClose: 1000,
            onClose: () => {
              window.location.href = "/login";
            },
          });
        }
      })
      .catch((err) => {
        dispatch({ type: "REGISTER_FAIL", payload: err.response.data });
      });
  };

export const loginUser = (email, password) => (dispatch) => {
  axios
    .post("http://localhost:4000/auth/login", {
      email,
      password,
    })
    .then((res) => {
      console.log(res.data);
      if (res.data.status === "success") {
        localStorage.setItem("token", res.data.token);
        dispatch({ type: "LOGIN_SUCCESS", payload: res.data.user });
        window.location.href = "/";
        toast.success(res.data.msg, {
          autoClose: 1000,
          onClose: () => {
            window.location.href = "/";
          },
        });
      } else {
        // alert();
        toast.error(res.data.msg, {
          autoClose: 1000,
        });
      }
    })
    .catch((err) => {
      dispatch({ type: "LOGIN_FAIL", payload: err.response.data });
    });
};

export const createProduct =
  (title, desc, category, price, image, attribute) => (dispatch) => {
    const token = localStorage.getItem("token");
    console.log(token);
    if (token) {
      axios
        .post(
          "http://localhost:4000/products/newProduct",
          {
            title: title,
            desc: desc,
            price: price,
            category: category,
            image: image,
            attribute: attribute,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then((res) => {
          console.log(res.data);
          if (res.data.status === "success") {
            toast.success(res.data.msg, {
              autoClose: 1000,
            });
          }
          if (res.data.status === false) {
            console.log(res.data.msg);
            toast.error(res.data.msg, { autoClose: 1000 });
          }
        })
        .catch((err) => {
          console.log(err.message);
        });
    }
  };

export const createCategory = (title, code) => (dispatch) => {
  const token = localStorage.getItem("token");
  if (token) {
    axios
      .post(
        "http://localhost:4000/category/addCategory",
        {
          title: title,
          code,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.status === "success") {
          toast.success("Add Category Success", {
            autoClose: 1000,
          });
        }
        if (res.data.status === false) {
          console.log(res.data.msg);
          toast.error(res.data.msg, { autoClose: 1000 });
        }
      })
      .catch((err) => {
        dispatch({ type: "REGISTER_FAIL", payload: err.response.data });
      });
  }
};
export const editCategory = (id, title, code) => (dispatch) => {
  const token = localStorage.getItem("token");
  if (token) {
    axios
      .put(
        `http://localhost:4000/category/edit/${id}`,
        {
          title,
          code,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.status === "success") {
          toast.success("Edit Category Success", {
            autoClose: 1000,
            onClose: () => {
              window.location.href = "/Admin/ListCategory";
            },
          });
        }
        if (res.data.status === false) {
          console.log(res.data.msg);
          toast.error(res.data.msg, { autoClose: 1000 });
        }
      })
      .catch((err) => {
        dispatch({ type: "REGISTER_FAIL", payload: err.response.data });
      });
  }
};

export const getAllProducts = async (page) => {
  try {
    const response = await axios.get(
      `http://localhost:4000/products/categories?page=${page}`
    );
    return response?.data.products;
  } catch (error) {
    console.log(error.message);
    return null;
  }
};
export const getAllCategory = async (page) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/category/getAll?page=${page}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response?.data.categorys;
    }
  } catch (error) {
    console.log(error.message);
    return null;
  }
};

export const getProductId = async (id) => {
  try {
    const response = await axios.get(`http://localhost:4000/products/${id}`);
    return response?.data?.product;
  } catch (error) {
    console.log(error.message);
    return null;
  }
};

export const getCategoryId = async (id) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(`http://localhost:4000/category/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response?.data.categoryID;
    }
  } catch (error) {
    console.log(error.message);
    return null;
  }
};

export const getProductByCategory = async (category) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/products/getCategory/${category}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response?.data.data;
    }
  } catch (error) {
    console.log(error.message);
    return null;
  }
};

export const addToCart = async (productId, user_id, quantity, attribute) => {
  const token = localStorage.getItem("token");
  try {
    if (token) {
      const response = await axios.post(
        "http://localhost:4000/cart/addCart",
        {
          productId,
          user_id,
          quantity,
          attribute,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response) {
        console.log(response);
        return response.data;
      }
    } else {
      toast.error("You need login", {
        autoClose: 1000,
        onClose: () => {
          window.location.href = "/SingIn";
        },
      });
    }
  } catch (error) {
    console.log(error.message);
    return null;
  }
};

export const increaseQuantityApi = async (idProductCart, idUser) => {
  const token = localStorage.getItem('token');
  try {
    const response = await axios.put(
      `http://localhost:4000/cart/increase/${idUser}`,
      {
        id: idProductCart
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response) {
      console.log(response);
      return response.data;
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const decreaseQuantityApi = async (idProductCart, idUser) => {
  const token = localStorage.getItem('token');
  try {
    const response = await axios.put(
      `http://localhost:4000/cart/decrease/${idUser}`,
      {
        id: idProductCart
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response) {
      console.log(response);
      return response.data;
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const deleteCartProductNew = async (idProductCart, idUser) => {
  const token = localStorage.getItem('token');
  console.log(idUser);
  try {
    const response = await axios.delete("http://localhost:4000/cart/delete", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        id: idProductCart,
        user_id: idUser,
      },
    });
    if (response) {
      return response.data;
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const deleteProductById = async (idProduct , pageCurrent) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.delete(
        `http://localhost:4000/products/deleteProduct?page=${pageCurrent}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: {
            id: idProduct,
          },
        }
      );
      if (response) {
        return response.data;
      }
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const deleteCategoryId = async (idCategory , pageCurrent) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.delete(
        `http://localhost:4000/category/delete?page=${pageCurrent}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: {
            id: idCategory,
          },
        }
      );
      if (response) {
        return response.data;
      }
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const getAllUsers = async (page) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/auth/getAll?page=${page}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response) {
        if (response.data.status == "success") {
          return response.data.allUsers;
        } else {
          return [];
        }
      }
    }
  } catch (error) {
    console.log(error.message);
    throw error;
  }
};

export const deleteAllCart = async (idUser) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = axios.delete("http://localhost:4000/cart/deleteAll", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          user_id: idUser,
        },
      });
      if (response) {
        return response.data;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const getUserByUserId = async (idUser) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const res = await axios.get(`http://localhost:4000/auth/${idUser}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (res.data.status === "success") {
        return res.data.user;
      } else {
        throw new Error("API returned unsuccessful status");
      }
    } else {
      throw new Error("Token not found in local storage");
    }
  } catch (error) {
    console.error(error);
    return null; // hoặc có thể return một message error khác tùy vào ý của bạn
  }
};

export const updateRole = async (idUser, role, pageCurrent) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const res = await axios.put(
        `http://localhost:4000/auth/role/${idUser}?page=${pageCurrent}`,
        {
          role,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (res.data.status === "success") {
        return res.data;
      } else {
        throw new Error("Api returned unsuccesful status");
      }
    } else {
      throw new Error("Token is not found in local storage");
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const updateInfo = async (formData) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const res = await axios.put(
        `http://localhost:4000/auth/info`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        } 
      );
      if (res.data.status === "success") {
        return res.data.user;
      } else {
        throw new Error("Api returned unsuccesful status");
      }
    } else {
      throw new Error("Token is not found in local storage");
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};
