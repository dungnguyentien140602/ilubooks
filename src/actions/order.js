import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const addNewOrder = async (user, items, totalPrice, shippingAddress) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.post(
        `http://localhost:4000/order/newOrder/${user}`,
        {
          user,
          items,
          totalPrice,
          shippingAddress,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data) {
        console.log(response.data);
        return response.data;
      }
    }
  } catch (error) {}
};

export const updateConfirm = async (orderId, note, shippingAddress, status) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.put(
        `http://localhost:4000/order/updateConfirm/${orderId}`,
        {
          shippingAddress,
          note,
          status,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data.status === "success") {
        return response.data.updateOrder;
      } else {
        toast.error(response.data.msg);
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const orderConfirms = async (userId) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/order/confirm?userId=${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data.status === "success") {
        console.log(response.data);
        return response.data.orderConfirm;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const deleteAllCart = async (idUser) => {
  try {
    const token = localStorage.getItem("token");
    const response = await axios.delete(
      "http://localhost:4000/cart/deleteAll",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
      {
        user_id: idUser,
      }
    );
    return response.data;
  } catch (error) {
    console.log(error.message);
  }
};

export const orderPendings = async (userId) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/order/pending?userId=${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data.status === "success") {
        return response.data.orderPending;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const orderProcessing = async (userId) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/order/processing?userId=${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data.status === "success") {
        return response.data.orderProcessing;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const orderCancelled = async (userId) => {
  try {
    console.log(userId);
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get(
        `http://localhost:4000/order/cancelled?userId=${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.data.status === "success") {
        return response.data.orderCancelled;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const allOrderAdminView = async () => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.get("http://localhost:4000/order/allOrder", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.data.status === "success") {
        return response.data.allOrder;
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const updateOrderByBody = async (orderId, statusbody) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.put(
        `http://localhost:4000/order/updateBody/${orderId}`,
        {
          status: statusbody,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data.data;
    }
  } catch (error) {}
};

export const deleteOrderByID = async (orderId) => {
  try {
    const token = localStorage.getItem("token");
    if (token) {
      const response = await axios.delete(
        `http://localhost:4000/order/delete/${orderId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data.allOrder;
    }
  } catch (error) {}
};
