import React from "react";
import { GiSelfLove } from "react-icons/gi";
import { SlHandbag } from "react-icons/sl";
import Navbar from "./Navbar";
import Link from "next/link";
import { useSelector } from "react-redux";
const Header = () => {
  const userData = useSelector((state) => state.auth);
  return (
    <div>
      <div className="bg-white">
        <div className="xl:px-[175px] mx-auto">
          <div className="pt-[18px] pb-[14px] sm:px-[30px] xl:px-0 flex flex-col md:flex-row items-center">
            {/* Title */}
            <div></div>
            <h4 className="text-[#111111] mb-[10px] md:mb-0 md:w-[18%] text-[24px] px-[15px] mt-[8px] font-[600]">
              ilubooks
            </h4>
            {/* Search */}
            <div className="md:flex-1 w-full h-[44px] px-[25px] overflow-hidden">
              <input
                type="text"
                name=""
                id=""
                className="w-full rounded-[20px] overflow-hidden h-full bg-[#ccc] outline-none px-[19px] text-[14px]"
                placeholder="Search on ilusbook.."
              />
            </div>
            {/* Options */}
            <div className="px-[40px] w-full items-center gap-[20px] mt-[20px] md:mt-0 md:w-[34%] flex">
              <div className="md:pr-[21px] text-[14px] w-[25%] flex items-center justify-center">
                <select className="border-none outline-none">
                  <option value="">US Dollar</option>
                  <option value="">RTGS Dollar</option>
                  <option value="">RTGS Dollar</option>
                </select>
              </div>
              <div className="pr-[21px] flex items-center gap-2">
                <span className="text-[14px] sm:text-[18px]">
                  {" "}
                  <GiSelfLove />
                </span>
                <span className="text-[14px]">WishList</span>
              </div>
              <Link
                href="/Cart"
                className="pr-[21px] relative flex items-center gap-2 text-[14px]"
              >
                <div className="text-[18px]">
                  <SlHandbag />
                  <div className="absolute w-[18px] h-[18px] bottom-[14px] left-[12px] rounded-full text-[12px] text-white font-[600] bg-red-500 flex items-center justify-center">
                {userData.user?.cart?.length}
                  </div>
                </div>
                <span>Cart</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <Navbar />
    </div>
  );
};

export default Header;
