import React from "react";

const CardSales = (props) => {
  return (
    <div
      className="px-[30px] py-[40px] relative rounded-xl"
      style={{
        backgroundImage: `url('${props.img}')`,
      }}
    >
      <div>
        <h2 className="uppercase text-white font-bold text-[24px]">
        {props.title}
        </h2>
        <h2 className="uppercase text-white font-bold text-[18px] tracking-widest">
          {props.desc}
        </h2>
        <button className="uppercase mt-[10px] text-white bg-[#f51167] text-[14px] font-[600] rounded-[10px] leading-normal text-center pt-[18px] pb-[14px] px-[47px]">
          shop now
        </button>
      </div>
      <div className="font-[500] pt-[5px] pb-[1px] uppercase absolute top-[27px] right-[26px] px-[9px] text-white text-[14px] bg-[#f51167] rounded-sm">
        Sale
      </div>
    </div>
  );
};

export default CardSales;