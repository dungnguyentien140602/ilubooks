
import React from "react";
import Link from "next/link";
import { BsInstagram } from "react-icons/bs";
import { BsTwitter } from "react-icons/bs";
import { BsYoutube } from "react-icons/bs";
import { BsLinkedin } from "react-icons/bs";
import { BsPinterest } from "react-icons/bs";
const Footer = () => {
  const Icons = [
    {
      data: <BsInstagram />,
      text: "Instagram",
    },

    {
      data: <BsPinterest />,
      text: "Pinterest",
    },
    // {
    //   data: <FaFacebookF />,
    //   text: "Facebook",
    // },
    {
      data:<BsTwitter/>,
      text:'Twitter'
    },
    {
      data:<BsYoutube/>,
      text:'Youtube'
    },
    {
      data:<BsLinkedin/>,
      text:'Linkedin'
    }
  ];
  const Contact = [
    {
      sign:'C.',
      data:'ilubooks'
    },
    {
      sign:'B.',
      data:'108 Chinhoyi Street, Central Business District, Harare Zimbabwe'
    },
    {
      sign:'T.',
      data:'+263782149840'
    },
    {
      sign:'E.',
      data:'rvmseamaf@gmail.com'
    }
  ]
  return (
    <div className=" w-full bg-[#282828] text-white absolute">
      <div className="xl:mx-[175px] bg-[#282828]">
      <div className="pt-[20px] mx-auto grid grid-cols-1 md:grid-cols-2 gap-6 lg:grid-cols-4">
        <div className="px-[40px] sm:px-[30px]">
          <h2 className="text-[18px] uppercase font-[700]">ABOUT</h2>
          <div className="mt-[35px]">
            <p className="text-[14px] text-[#8F8F8F] mb-[25px] tracking-tighter">
              Online & physical bead shop with the best beads and beading
              supplies in Zimbabwe ✓ Over 9000 beads for jewelry making ✓ Glass
              beads ✓ Beading supplies and much more!
            </p>
            <div>
              <img src="https://seamaf.com/frontend/img/cards.png" alt="" />
            </div>
            <div className="rounded-md overflow-hidden mt-[15px] bg-white border-[2px] border-[#f0f0f0] w-[90%] py-[6px] px-[10px] flex justify-between items-center">
              <input
                type="text"
                placeholder="Enter e-mail"
                className="text-[12px] text-[#495057] border-none outline-none"
              />
              <button className="text-[12px] text-[#F51167] uppercase font-[700] border-none outline-none">
                Subscrie
              </button>
            </div>
          </div>
        </div>
        <div className="px-[40px] sm:px-[30px]">
          <h2 className="text-[18px] uppercase font-[700]">useful links</h2>
          <div className="mt-[35px]  grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2">
            <ul className="text-[14px] flex flex-col items-start list-disc text-[#8F8F8F] md:pl-[15px]">
              <Link href='#' className="mb-[6px] hover:text-[#fff]">About Us</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Track Orders</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Shipping</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Contact</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">My Orders</Link>
            </ul>
            <ul className="text-[14px] flex flex-col items-start list-disc text-[#8F8F8F] md:pl-[15px]">
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Support</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Terms of Use</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Privacy Policy</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Our Services</Link>
              <Link href='#' className="mb-[6px] hover:text-[#fff]">Blog</Link>
            </ul>
          </div>
        </div>
        <div className="px-[40px] sm:px-[30px]">
          <h2 className="text-[18px] uppercase font-[700]">blog</h2>
          <div className="mt-[35px] flex gap-[22px]">
            <img
              src="https://seamaf.com/storage/uploads/blog/zs5BpOd01zzl3q.jpg"
              alt=""
              className="w-[64px] h-[64px] object-cover"
            />

            <div>
              <h2 className="text-[18px] uppercase font-[500]">
                Bohe mian wedding theme
              </h2>
              <span className="mt-[4px] text-[12px] text-[#8F8F8F]">
                2 years ago
              </span>
              <p className="mt-[8px] text-[#F51167] font-bold text-[12px]">
                Read More
              </p>
            </div>
          </div>
          <div className="mt-[35px] flex gap-[22px]">
            <img
              src="https://seamaf.com/storage/uploads/blog/kQbaFz3Mul2782.jpg"
              alt=""
              className="w-[64px] h-[64px] object-cover"
            />

            <div>
              <h2 className="text-[18px] uppercase font-[500]">
                Vintage Wedding Theme ``
              </h2>
              <span className="mt-[4px] text-[12px] text-[#8F8F8F]">
                2 years ago
              </span>
              <p className="mt-[8px] text-[#F51167] font-bold text-[12px]">
                Read More
              </p>
            </div>
          </div>
        </div>
        <div className="px-[40px] sm:px-[30px]">
          <h2 className="text-[18px] uppercase font-[700]">contact</h2>
          <div className="mt-[35px]">
            {
              Contact.map((item , index) => (
                <div className="flex items-start mb-[15px] gap-[15px]">
                   <h2 className="uppercase text-[#F51167]">{item.sign}</h2>
                   <span className="text-[14px] text-[#8F8F8F] tracking-tighter">{item.data}</span>
                </div>
              ))
            }
          </div>
        </div>
      </div>
      {/* Social */}
      <div className="w-full mt-[50px] px-[40px] sm:px-[30px] border-t-[1px] border-[#8F8F8F]">
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-col-4 gap-[20px] mt-[10px]">
          {Icons.map((item, data) => (
            <div key={item} className="flex items-center gap-[15px]">
              <span className="text-[26px] text-[#9F9FA0]">
                 {item.data}
              </span>
              <span className="uppercase text-[12px] font-[600] text-[#9F9FA0]">
                {item.text}
              </span>
            </div>
          ))}
        </div>
        <div className="flex items-center justify-center mt-[50px]">
          <p className="text-[14px] text-[#ffffff] mb-[16px]">
            Copyright ©2023 All rights reserved | Developed By Eloquent Geeks
          </p>
        </div>
      </div>
      </div>
    </div>
  );
};

export default Footer;
