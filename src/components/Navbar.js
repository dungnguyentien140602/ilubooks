import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getAllCategory } from "@/actions/userActions";
const Navbar = () => {
  const userData = useSelector((state) => state.auth);
  const router = useRouter();
  console.log(userData.user?.role);
  const [dataCategory, setDataCategory] = useState([]);
  const dispatch = useDispatch();
  const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;
  const items = [
    {
      key: "1",
      label: "This is panel header 1",
      children: <p>{text}</p>,
    },
    {
      key: "2",
      label: "This is panel header 2",
      children: <p>{text}</p>,
    },
    {
      key: "3",
      label: "This is panel header 3",
      children: <p>{text}</p>,
    },
  ];
  const handleLogout = () => {
    dispatch({ type: "LOG_OUT", payload: [] });
    toast.success("Log out successfully", {
      autoClose: 1000,
    });
  };

  const handleDashboard = () => {
    if (userData.user?.role === 'user') {
      router.push("/Admin/ListProduct");
    } else {
      toast.error("You not Admin", {
        autoClose: 1000,
      });
    }
  };

  useEffect(() => {
    getAllCategory()
      .then((data) => setDataCategory(data))
      .catch((err) => console.log(err.message));
  }, []);

  return (
    <div className="py-[17px] xl:px-[175px] mx-auto w-full bg-[#282828] ">
      <div className="hidden xl:px-[15px] px-[40px] flex-wrap md:flex items-center text-[16px] text-white gap-[30px] ">
        <Link href="/" className="cursor-pointer hover:text-[#f51167]">
          Home
        </Link>

        <div class="dropdown dropdown-hover z-50">
          <Link href="/" className="cursor-pointer hover:text-[#f51167]">
            Our Shop
          </Link>
          <ul
            tabindex="0"
            class="dropdown-content menu  p-2 shadow bg-white text-black rounded-none w-52"
          >
            {dataCategory?.length > 0 ? (
              dataCategory.map((item, index) => (
                <li key={index}>
                  <a>{item.title}</a>
                </li>
              ))
            ) : (
              <li>Nothing Category</li>
            )}
          </ul>
        </div>
        <Link
          href="/shop-categories"
          className="cursor-pointer relative hover:text-[#f51167]"
        >
          On Sale
        </Link>
        <Link href="/" className="cursor-pointer hover:text-[#f51167]">
          Our Service
        </Link>
        <Link href="/" className="cursor-pointer hover:text-[#f51167]">
          Blog
        </Link>
        <Link href="/" className="cursor-pointer hover:text-[#f51167]">
          Contact
        </Link>
        <p
          onClick={handleDashboard}
          className="cursor-pointer hover:text-[#f51167]"
        >
          Dashboard
        </p>
        {userData.user ? (
          <div className="flex items-center gap-[54px]">
            <div class="dropdown dropdown-hover z-50">
              <Link href="/" className="cursor-pointer hover:text-[#f51167]">
                {userData.user?.name}
              </Link>
              <ul
                tabindex="0"
                class="dropdown-content menu  p-2 shadow bg-white text-black rounded-none w-52"
              >
                <li>
                  <Link href='/user/profile'>Tài khoản của tôi</Link>
                </li>
                <li>
                  <Link href='/user/purchase/all'>Đơn Mua</Link>
                </li>
              </ul>
            </div>
            <span
              className="cursor-pointer hover:text-[#f51167]"
              onClick={handleLogout}
            >
              LogOut
            </span>
          </div>
        ) : (
          <div className="flex items-center gap-[54px]">
            {" "}
            <Link
              href="/SingIn"
              className="cursor-pointer hover:text-[#f51167]"
            >
              SignIn
            </Link>
            <Link
              href="/SignUp"
              className="cursor-pointer hover:text-[#f51167]"
            >
              SignUp
            </Link>
          </div>
        )}
      </div>
      <div className="md:hidden py-[10px] flex justify-end px-[20px]">
        <details style={{ borderRadius: "0px" }} className="collapse">
          <summary className="collapse-title text-end px-[8px] py-[4px] border-none border-[1px] text-white text-[16px] font-medium">
            MENU
          </summary>
          <div className="collapse-content text-white w-full bg-[#282828] flex flex-col">
            <Link
              href="/"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Home
            </Link>
            <Link
              href="/"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Our Shop
            </Link>
            <Link
              href="/shop-categories"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              On Sale
            </Link>
            <Link
              href="/"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Our Service
            </Link>
            <Link
              href="/"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Blog
            </Link>
            <Link
              href="/"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Contact
            </Link>
            <Link
              href="/Admin/ListProduct"
              className="cursor-pointer py-[5px] px-[10px] hover:bg-slate-400 rounded-md"
            >
              Dashboard
            </Link>
          </div>
        </details>
      </div>
    </div>
  );
};

export default Navbar;
