import React from 'react'

const BreadCrumb = () => {
  return (
    <div className='pt-[20px] pb-[10px] bg-[#f8f7f7] px-[190px]'>
        <h1 className='text-[24px] font-bold text-[#414141] uppercase'>product on sale</h1>
        <a className='text-[14px] text-[#414141] font-semibold'>Home / Shop</a>
    </div>
  )
}

export default BreadCrumb