"use client"
import React, { useState } from "react";
import { AiFillHome , AiFillFolderAdd , AiOutlineEdit} from "react-icons/ai";
import {TbCategory} from 'react-icons/tb'
import {BsListCheck , BsDatabaseAdd , BsCreditCard2Back} from 'react-icons/bs'
import {BiUser} from 'react-icons/bi'
import Link from "next/link";
import { useSelector } from "react-redux";
const AdminNav = () => {
  const ProductPath = [
    {
      id:1,
      path:"/Admin/ListProduct",
      icon:<BsListCheck className="text-[14px] text-[#FFFFFFB3]"/>,
      title:"List Product"
    },
    {
      id:2,
      path:"/Admin/AddProduct",
      icon:<AiFillFolderAdd className="text-[14px] text-[#FFFFFFB3]"/>,
      title:"Add Product"
    },
  ]
  const CategoryPath = [
    {
      id:4,
      path:"/Admin/ListCategory",
      icon:<TbCategory className="text-[14px] text-[#FFFFFFB3]"/>,
      title:"List Category"
    },
    {
      id:5,
      path:"/Admin/AddCategory",
      icon:<BsDatabaseAdd className="text-[14px] text-[#FFFFFFB3]"/>,
      title:"Add Category"
    },
  ]
  const Order = [
    {
      id:6,
      path:"/Admin/Order/pending",
      icon:<TbCategory className="text-[14px] text-[#FFFFFFB3]"/>,
      title:"List Order"
    },  
  ]
  const [checkColor , setCheckColor] = useState();
  const handleChangeColor = (id) => {
    setCheckColor(id)
    console.log(checkColor);
  }
  
  return (
    <div className="w-[18%] min-h-[100vh] bg-[#081627]">
      <div className="px-[24px] py-[12px] bg-[#081627]">
        <h1 className="text-white text-[24px] font-bold">ILUSBOOK</h1>
      </div>
      <div className="px-[24px] py-[12px] bg-[#081627] border-t-[1px] border-b-[1px] border-[#f1f1f1]">
        <div className="flex items-center gap-3">
          <AiFillHome className="text-[14px] text-[#FFFFFFB3]" />
          <h2 className="text-[14px] text-[#FFFFFFB3]">ILusBook Overview</h2>
        </div>
      </div>
      {/* Product */}
      <div className="border-b-[1px] bg-[#101f33] border-[#f1f1f1]">
        <div className="px-[24px] py-[16px] bg-[#081627]">
          <h1 className="text-[14px] text-white">Product</h1>
        </div>
        <div className="pb-[20px] flex flex-col">
          {
            ProductPath.map((item , index) => (
              <Link href={item.path} key={index} onClick={() => handleChangeColor(item.id)} className= {checkColor == item.id ? 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3] text-blue-400' : 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3]'} >
                {item.icon}
                {item.title}</Link>
            ))
          }
        </div>
      </div>
      
      {/* Auth */}
      <div className="bg-[#101f33]">
        <div className="px-[24px] py-[16px] bg-[#081627]">
          <h1 className="text-[14px] text-white">Category</h1>
        </div>
        <div className="pb-[20px] flex flex-col">
          {
            CategoryPath.map((item , index) => (
              <Link
              href={item.path}
              key={index}
              onClick={() => handleChangeColor(item.id)} 
              className= {checkColor == item.id ? 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3] text-blue-400' : 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3]'}
            >
                  {item.icon}
               {item.title}
            </Link>
            ))
          }
         
        </div>
      </div>
      <div className="bg-[#101f33]">
        <div className="px-[24px] py-[16px] bg-[#081627]">
          <h1 className="text-[14px] text-white">Auth</h1>
        </div>
        <div className="pb-[20px] flex flex-col">
          <Link
            href="/Admin/ListUsers"
            className="flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3]"
          >
                <BiUser className="text-[14px] text-[#FFFFFFB3]" />
           List User
          </Link>
        </div>
      </div>
      <div className="bg-[#101f33]">
        <div className="px-[24px] py-[16px] bg-[#081627]">
          <h1 className="text-[14px] text-white">Order</h1>
        </div>
        <div className="pb-[20px] flex flex-col">
          {
            Order.map((item , index) => (
              <Link
              href={item.path}
              key={index}
              onClick={() => handleChangeColor(item.id)} 
              className= {checkColor == item.id ? 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3] text-blue-400' : 'flex items-center gap-3 font-medium text-[14px] hover:bg-[#ccc] px-[24px] py-[8px] text-[#FFFFFFB3]'}
            >
                  {item.icon}
               {item.title}
            </Link>
            ))
          }
         
        </div>
      </div>
    </div>
  );
};

export default AdminNav;
