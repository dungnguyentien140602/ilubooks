import React from 'react'
import {BsStars} from 'react-icons/bs'
const Feature = () => {
  return (
    <div className='mx-auto bg-[#f8f8f8]'>
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3'>
             <div className='py-[20px] px-[25px] flex items-center justify-center gap-[15px]'>
                <img src='https://seamaf.com/frontend/img/icons/1.png' alt="my-bag" className='w-[30px] h-[23px] object-cover' />
                <span className='text-[16px] text-[#111111] font-bold'>Fast Secure Payments</span>
             </div>
             <div className='py-[20px] px-[25px] flex items-center bg-[#f51167] justify-center gap-[15px]'>
               <img src="https://seamaf.com/frontend/img/icons/2.png" className='w-[30px] h-[30px] object-cover' alt="" />
                <span className='text-[16px] text-[#ffffff] font-bold'>Prenium Products</span>
             </div>
             <div className='py-[20px] px-[25px] flex items-center justify-center gap-[15px]'>
                <img src="https://seamaf.com/frontend/img/icons/3.png" className='w-[30px] h-[30px] object-cover' alt="" />
                <span className='text-[16px] text-[#111111] font-bold'>Affordable Delivery</span>
             </div>
        </div>
    </div>
  )
}

export default Feature