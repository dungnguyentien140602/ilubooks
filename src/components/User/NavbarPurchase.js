"use client"
import Link from 'next/link'
import React, { useEffect } from 'react'
import { usePathname } from 'next/navigation'
import { useState } from 'react'
const NavbarPurchase = () => {
  const [active , setActive] = useState('')
  const pathname = usePathname();
  useEffect(() =>{
    console.log(pathname);
    setActive(pathname)
  },[pathname])
  const LinksRoute = [
    {
      href:'/user/purchase/all',
      title:'Tất cả'
    },
    {
      href:'/user/purchase/waitpay',
      title:'Chờ thanh toán'
    },
    {
      href:'/user/purchase/processing',
      title:'Vận chuyển'
    },
    {
      href:'#',
      title:'Đang giao'
    }, {
      href:'#',
      title:'Hoàn thành'
    }
  ]
  return (
    <div className='flex items-center bg-white shadow-md text-[18px] mb-[20px]'>
      {
        LinksRoute.map((item , index) => (
          <Link href={item.href} className={active === item.href ? 'text-[#f51167] hover:text-[#f51167] px-[30px] border-b-[1px] border-[#f51167] py-[10px]' :'px-[30px] py-[10px]'}>{item.title}</Link>
        ))
      }
    </div>
  )
}

export default NavbarPurchase