"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { AiOutlineUser } from "react-icons/ai";
import { GrNotes } from "react-icons/gr";
import { HiOutlineKey } from "react-icons/hi";
import { useSelector } from "react-redux";
const Sidebar = () => {
  const [active, setActive] = useState("");
  const pathname = usePathname();
  const userData = useSelector((state) => state.auth);
  useEffect(() => {
    console.log(pathname);
    setActive(pathname);
  }, [pathname]);
  const LinksRoute = [
    {
      href: "/user/profile",
      title: "Tài khoản của tôi ",
      icon: <AiOutlineUser />,
    },
    {
      href: "/user/password",
      title: "Đổi mật khẩu",
      icon: <HiOutlineKey />,
    },
    {
      href: "/user/purchase/all",
      title: "Đơn mua",
      icon: <GrNotes />,
    },
  ];
  return (
    <div className="w-[22%]">
      {/* info user */}
      <div className="flex items-center gap-3 border-b-[1px] border-[#00000020] pb-[10px]">
        <img
          className="w-[50px] h-[50px] rounded-[50%]"
          src={`http://localhost:4000/${userData.user.image}`}
        ></img>
        <div>
          <h2 className="text-[16px] font-medium">Nguyễn tiến dũng</h2>
        </div>
      </div>
      <div className="pt-[30px] flex flex-col gap-4">
        {LinksRoute.map((item, index) => (
          <Link
            key={index}
            href={item.href}
            className={active === item.href ? 'flex text-[20px] items-center gap-2 text-[#f51167] hover:text-[#f51167] ' :'flex text-[20px] items-center gap-2 '}
          >
            {item.icon}
            <h1 className="text-[16px] font-[350]">{item.title}</h1>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
