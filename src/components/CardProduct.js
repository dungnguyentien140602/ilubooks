import Link from 'next/link'
import React from 'react'

const CardProduct = (props) => {
  function formatCurrency(num) {
    if(num){
      return num.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
    }
    return null
  }
  return (
    <Link href={{ pathname: `/product/${props.id}`}} className='cursor-pointer'>
        <img src={`http://localhost:4000/${props.img}`} className='w-full h-[300px] object-cover' alt="" />
        <div className='flex justify-between pt-[20px] w-full'>
            <span className='w-[163px] text-[14px] h-[18.2px] line-clamp-1 overflow-hidden text-ellipsis text-[#111111]'>{props.title}</span>
            <p className='text-[16px] font-[600] text-[#111111]'>{formatCurrency(props.price)}</p>
        </div>
    </Link>
  )
}

export default CardProduct