"use client";
import {
  getAllCategory,
  getAllProducts,
  getProductByCategory,
} from "@/actions/userActions";
import CardProduct from "@/components/CardProduct";
import { useParams, useRouter, useSearchParams } from "next/navigation";
import React, { useEffect, useState, useCallback } from "react";
import Link from "next/link";
import axios from "axios";

const page = () => {
  const [categories, setDataCategory] = useState([]);
  const [productCategory, setProductCategory] = useState([]);
  const [valueMin, setValueMin] = useState(100000);
  const [valueMax, setValueMax] = useState(200000);
  const [page, setPage] = useState(1);
  const params = useSearchParams();
  const pageCurrent = params.get("page") || 1;
  console.log(pageCurrent);
  console.log(params.get("pagesize"));
  const paramsId = useParams();
  const router = useRouter();
  console.log(paramsId.Category);
  useEffect(() => {
    getAllCategory()
      .then((data) => {
        console.log(data);
        setDataCategory(data.docs);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);
  const getDataCategoryPagi = async () => {
    try {
      const response = await axios.get(
        `http://localhost:4000/products/getCategory/${paramsId.Category}?page=${pageCurrent}`
      );
      console.log(response.data);
      setProductCategory(response.data.pageCategoryPagi);
    } catch (error) {
      console.log(error.message);
    }
  };
  useEffect(() => {
    getDataCategoryPagi();
  }, [pageCurrent]);
  const handleValueMin = () => {
    if (valueMin <= 100000) {
      setValueMin(1);
    } else {
      setValueMin(valueMin + 1);
    }
  };
  const handleValueMinIncrease = () => {
    setValueMin(valueMin + 1);
  };
  const handleValueMaxDecrease = () => {
    if (valueMax <= 200000) {
      setValueMax(1);
    } else {
      setValueMax(valueMax - 1);
    }
  };
  const handleValueMaxIncrease = () => {
    setValueMax(valueMax + 1);
  };
  const handleFilterProduct = () => {
    const filterProduct = productCategory.filter((product) => {
      return product.price >= valueMin && product.price <= valueMax;
    });
    setProductCategory(filterProduct);
  };
  const PrevPage = () => {
    setPage(page - 1);
    if (page <= 1) {
      setPage(1);
    } else {
      router.push(`/Category/${paramsId.Category}?page=${page - 1}`);
    }
  };
  const NextPage = () => {
    setPage(page + 1);
    router.push(`/Category/${paramsId.Category}?page=${page + 1}`);
  };
  return (
    <div>
      <div className="bg-slate-100">
        <div className="xl:mx-[175px]">
          <div className="mx-auto px-[15px] py-[15px]">
            <h1 className="text-[26px] font-bold">Findings</h1>
            <h2>Home / Shop</h2>
          </div>
        </div>
      </div>
      <div className="xl:mx-[175px] my-[40px]">
        <div className="px-[15px] md:flex gap-4 items-start">
          <div className="w-full md:w-[25%]">
            <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
              categories
            </h1>
            <ul className="flex flex-col">
              {categories?.map((item, index) => (
                <Link
                  href={`/Category/${item.code}`}
                  key={index}
                  className="cursor-pointer hover:font-medium hover:text-[#f51167] pt-[12px] pb-[5px] text-[12px] text-[#414141] border-b-[1px] border-[#00000020]"
                >
                  {item.title}
                </Link>
              ))}
            </ul>
            <div className="mt-[40px] py-[30px] border-b-[1px] border-[#00000020]">
              <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
                refine by
              </h1>
              <div className="flex items-center gap-3">
                <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
                  min price
                </h1>
                <div className="w-[140px] px-[15px] h-[40px] mb-[40px] rounded-[25px] border-[1px] border-[#00000020] flex items-center gap-2">
                  <button onClick={handleValueMin} className="w-[20px]">
                    -
                  </button>
                  <input type="number" className="w-[72px]" value={valueMin} />
                  <button onClick={handleValueMinIncrease} className="w-[20px]">
                    +
                  </button>
                </div>
              </div>
              <div className="flex items-center gap-3">
                <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
                  max price
                </h1>
                <div className="w-[140px] px-[15px] h-[40px] mb-[40px] rounded-[25px] border-[1px] border-[#00000020] flex items-center gap-2">
                  {" "}
                  <button
                    type="button"
                    onClick={handleValueMaxDecrease}
                    className="w-[20px]"
                  >
                    -
                  </button>
                  <input type="number" className="w-[72px]" value={valueMax} />
                  <button
                    type="button"
                    onClick={handleValueMaxIncrease}
                    className="w-[20px]"
                  >
                    +
                  </button>
                </div>
              </div>
              <button
                onClick={handleFilterProduct}
                className=" rounded-md overflow-hidden px-[67px] py-[14px] bg-[#f51167] text-white font-bold uppercase"
              >
                filter
              </button>
            </div>
          </div>

          <div className="grid sm:grid-cols-2 md:grid-cols-3 gap-[30px]">
            {productCategory.map((item, index) => (
              <CardProduct
                id={item._id}
                key={index}
                img={item.image}
                desc={item.desc}
                price={item.price}
              />
            ))}
          </div>

         
        </div>
        <div className="flex justify-start ml-[350px] mr-[40px] h-[30px] my-[30px]">
            <div className="flex items-center gap-1">
              <button
                onClick={PrevPage}
                className="border-[1px] border-[#00000020] px-3 rounded-md py-2"
              >
                «
              </button>

              <button
                onClick={NextPage}
                className="border-[1px] border-[#00000020] px-3 rounded-md py-2"
              >
                »
              </button>
            </div>
          </div>
      </div>
    </div>
  );
};

export default page;
