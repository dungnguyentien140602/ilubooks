"use client";
import React, { useEffect, useState, useCallback, useMemo } from "react";
import CardProduct from "@/components/CardProduct";
import { FaTimes } from "react-icons/fa";
import axios from "axios";
import {
  increaseQuantityApi,
  decreaseQuantityApi,
  deleteCartProductNew,
} from "@/actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import { addNewOrder } from "@/actions/order";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
const page = () => {
  const [dataAll, setDataAll] = useState([]);
  const userData = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [checkAll, setCheckAll] = useState(false);
  const router = useRouter();
  const [totalPricenew, setTotalPrice] = useState(0);
  const token = localStorage.getItem("token");
  const [selectedItems, setSelectedItems] = useState([]);
  const getData = useCallback(async () => {
    try {
      const response = await axios.get(
        `http://localhost:4000/cart/getAll/${userData.user?._id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      console.log(response?.data.cartUser);
      console.log(response?.data.totalPrice);
      setDataAll(response?.data);
    } catch (error) {
      console.log(error.message);
    }
  }, [token]);
  useEffect(() => {
    if (token) {
      getData();
    }
  }, []);

  const getPriceProductNumber = useMemo(() => {
    return (number, value) => {
      return number * value;
    };
  }, []);
  const inCreaseQuantity = (idProductCart) => {
    const userId = userData?.user._id;
    console.log(userId);
    console.log(idProductCart);
    increaseQuantityApi(idProductCart, userId)
      .then((data) => setDataAll(data))
      .catch((err) => console.log(err.message));
  };

  const decreaseQuantity = (idProductCart) => {
    const userId = userData?.user._id;
    console.log(userId);
    console.log(idProductCart);
    decreaseQuantityApi(idProductCart, userId)
      .then((data) => setDataAll(data))
      .catch((err) => console.log(err.message));
  };
  const deleteCartProduct = (idProductCart) => {
    const userId = userData?.user._id;
    console.log(userId);
    console.log(idProductCart);
    deleteCartProductNew(idProductCart, userId)
      .then((data) => {
        console.log(data);
        setDataAll(data);
      })
      .catch((error) => console.log(error.message));
  };
  const formatCurrency = useMemo(() => {
    return (num) => {
      if (num) {
        return num.toLocaleString("vi-VN", {
          style: "currency",
          currency: "VND",
        });
      }
      return 0;
    };
  }, []);

  const handleSelectedItems = (data) => {
    console.log(data);
    const index = selectedItems.findIndex((item) => item._id === data._id);
    console.log(index);
    if (index !== -1) {
      // Nếu sản phẩm đã được check, ta sẽ xóa nó đi
      const newSelectedItems = [
        ...selectedItems.slice(0, index),
        ...selectedItems.slice(index + 1),
      ];
      setSelectedItems(newSelectedItems);
    } else {
      const newItem = {
        product_id: data.productId._id,
        attributes: data.attribute,
        quantity: data.quantity,
        price: data.productId.price,
        _id: data._id,
      };
      console.log(newItem);
      setSelectedItems([...selectedItems, newItem]);
    }
  };

  const handleCheckAll = () => {
    const selectedCount = selectedItems.length;
    const totalCount = dataAll.cartUser.length;

    if (selectedCount === totalCount) {
      setCheckAll(false);
      setSelectedItems([]);
    } else {
      const checkItems = dataAll.cartUser.map((item) => ({
        product_id: item.productId._id,
        attributes: item.attribute,
        quantity: item.quantity,
        price: item.productId.price,
        _id: item._id,
      }));
      setCheckAll(true);
      setSelectedItems(checkItems);
    }
  };

  useEffect(() => {
    let totalPrice = 0;
    selectedItems.forEach((item) => {
      const price = getPriceProductNumber(item.quantity, item.price);
      totalPrice += price;
    });
    setTotalPrice(totalPrice);
    setCheckAll(selectedItems.length===dataAll.cartUser?.length)
  }, [selectedItems]);

  const hadleCheckout = () => {
    if (selectedItems.length == 0) {
      toast.error("Vui lòng chọn sản phẩm");
    } else {
      addNewOrder(
        userData.user._id,
        selectedItems,
        totalPricenew,
        "Hà Nội , Jakata"
      );
      router.push("/Checkout");
      toast.success("Tạo đơn hàng thành công", {
        autoClose: 1000,
      });
    }
  };
  return (
    <div className="xl:px-[175px]  mx-auto">
      <div className="px-[40px] md:px-[30px] mt-[100px] mb-[60px] flex-col flex md:flex-row gap-[30px]">
        <div className="md:w-[60%] rounded-[30px] overflow-hidden">
          <div className="px-[34px] pt-[40px] bg-[#f0f0f0] pb-[40px]">
            <h1 className="mb-[37px] text-[30px] text-[#111111] font-[700]">
              Your Cart
            </h1>
            <div className="flex items-center">
              <h1 className="ml-[10px] w-[52%] text-[16px]">Product</h1>
              <input type="checkbox" checked={checkAll} onChange={handleCheckAll}></input>

              <div className="px-[10px] flex-1 flex justify-between items-center">
                <h1 className="text-[16px]">Quantity</h1>
                <h1 className="text-[16px]">Attribute</h1>
                <h1 className="text-[16px]">Price</h1>
              </div>
            </div>
            {dataAll.cartUser?.length > 0 ? (
              dataAll.cartUser.map((item, index) => (
                <div key={index} className="flex items-center mt-[30px]">
                  <input
                    type="checkbox"
                    checked={selectedItems.some(
                      (items) => items._id === item._id
                    )}
                    onChange={() => handleSelectedItems(item)}
                  ></input>
                  <div className="w-[52%] flex gap-[10px]">
                    <img
                      src={`http://localhost:4000/${item.productId?.image}`}
                      className="w-[73px] h-[73px] overflow-hidden"
                      alt=""
                    />
                    <div>
                      <h3 className="w-[200px] h-[20.8px] line-clamp-1 overflow-hidden text-ellipsis text-[16px] text-[#414141] font-[600] mb-[3px]">
                        {item.productId?.title}
                      </h3>
                      <span className="text-[16px] text-[#414141]">
                        {formatCurrency(item.productId?.price)}
                      </span>
                    </div>
                  </div>
                  <div className="flex-1 justify-between flex items-center">
                    <div className="flex w-[100px] py-[5px] px-[8px] items-center rounded-[25px] overflow-hidden bg-white">
                      <button
                        onClick={() => decreaseQuantity(item._id)}
                        className="w-[10px]"
                      >
                        -
                      </button>
                      <div className="flex flex-1 items-center justify-center">
                        <span>{item.quantity}</span>
                      </div>
                      <button
                        onClick={() => inCreaseQuantity(item._id)}
                        className="w-[15px]"
                      >
                        +
                      </button>
                    </div>
                    <div className="flex items-center gap-2">
                      {item.attribute.map((itemAtr, index) => (
                        <p key={index} className="text-[16px] font-[600]">
                          {itemAtr.value}
                        </p>
                      ))}
                    </div>
                    <div className="flex items-center gap-3">
                      <p className="text-[16px] font-[400]">
                        {formatCurrency(
                          getPriceProductNumber(
                            item.quantity,
                            item.productId?.price
                          )
                        )}
                      </p>
                      <FaTimes
                        onClick={() => deleteCartProduct(item._id)}
                        className="text-red-500 text-[18px] cursor-pointer"
                      />
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <h1 className="text-[20px] text-black">
                Nothing Product In Cart
              </h1>
            )}
          </div>
          <div className="px-[34px] flex justify-end rounded-b-[30px] py-[22px] bg-[#f51167]">
            <div className="flex items-center">
              <h3 className="text-[18px] font-[700] text-[#ffffff] mr-[30px]">
                Total
              </h3>
              <h3 className="text-[18px] font-[700] text-[#ffffff]">
                {formatCurrency(totalPricenew)}
              </h3>
            </div>
          </div>
        </div>
        <div className="flex-1">
          <div>
            <div className="w-full px-[12px] h-[40px] sm:px-[24px] sm:h-[58px] rounded-[30px] flex items-center border-[1px] border-[#00000020]">
              <input
                type="text"
                className="text-[16px] h-full w-[80%] border-none outline-none"
                placeholder="Enter promo code"
              />
              <button
                type="submit"
                className="text-[#f51167] text-[16px] font-[700] uppercase"
              >
                SUBMIT
              </button>
            </div>
          </div>
          {/* Shipping Options */}
          <div className="px-[30px] py-[10px] border-[1px] rounded-xl mt-[20px] border-[#00000020]">
            <h1 className="uppercase text-[#111111] text-[16px] font-[700]">
              shipping options
            </h1>
            <div className="flex mt-[20px] items-center gap-[20px]">
              <h6 className="w-[55%] text-[#111111] font-[700] text-[16px]">
                Select zone
              </h6>
              <select
                name=""
                id=""
                className="w-full  border-[1px] outline-none py-[6px]"
              >
                <option value="1">Kamisa</option>
                <option value="2">Elsa</option>
                <option value="3">Weteghost</option>
              </select>
            </div>
            <button className="mt-[20px] rounded-md bg-[#111111] text-white flex items-center text-center py-[6px] px-[12px]">
              Calculate
            </button>
          </div>
          <button
            onClick={hadleCheckout}
            className="flex items-center justify-center text-[14px] text-white font-[700] px-[20px] sm:px-[47px] pt-[12px] sm:pt-[23px] w-full mt-[20px] rounded-md pb-[14px] uppercase bg-[#f51167]"
          >
            Proceed to checkout
          </button>
          <button className="flex items-center justify-center text-[14px] text-white font-[700]  px-[20px] sm:px-[47px] pt-[12px] sm:pt-[23px] w-full mt-[15px] rounded-md pb-[14px] uppercase bg-[#111111]">
            Continue Shopping
          </button>
        </div>
      </div>
    </div>
  );
};

export default page;
