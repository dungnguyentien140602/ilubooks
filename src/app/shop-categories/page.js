"use client";
import {
  getAllCategory,
  getAllProducts,
  getProductByCategory,
} from "@/actions/userActions";
import CardProduct from "@/components/CardProduct";
import Link from "next/link";
import { useParams, useRouter, useSearchParams } from "next/navigation";
import React, { useEffect, useState, useCallback } from "react";

const page = () => {
  const [categories, setCategories] = useState([]);
  const [productCategory, setProductCategory] = useState([]);
  const [valueMin, setValueMin] = useState(100000);
  const [valueMax, setValueMax] = useState(200000);
  const params = useSearchParams();
  const page = params.get('pagesize') || 1;
  const router = useRouter();

  useEffect(() => {
    getAllCategory()
      .then((data) => {
        console.log(data);
        setCategories(data.docs);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  useEffect(() => {
    getAllProducts(page)
      .then((data) => {
        setProductCategory(data.docs);
      })
      .catch((err) => console.log(err.message));
  }, [page]);

  const handleValueMin = () => {
    setValueMin((prevValue) => (prevValue <= 100000 ? 1 : prevValue + 1));
  };

  const handleValueMinIncrease = () => {
    setValueMin((prevValue) => prevValue + 1);
  };

  const handleValueMaxDecrease = () => {
    setValueMax((prevValue) => (prevValue <= 200000 ? 1 : prevValue - 1));
  };

  const handleValueMaxIncrease = () => {
    setValueMax((prevValue) => prevValue + 1);
  };

  const handleFilterProduct = () => {
    const filterProduct = productCategory.filter((product) => {
      return product.price >= valueMin && product.price <= valueMax;
    });
    setProductCategory(filterProduct);
  };

  const NextPage = () => {
    router.push(`/shop-categories/?pagesize=${parseInt(page) + 1}`);
  };
  const PrevPage = () => {
     if(page == 1 ){
      console.log(page);
     }else{
      router.push(`/shop-categories/?pagesize=${parseInt(page) - 1}`);
     }
  };
  return (
    <div className="mx-[175px] my-[40px]">
      <div className="px-[15px] md:flex gap-4 items-start">
        <div className="w-full md:w-[25%]">
          <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
            categories
          </h1>
          <ul className="flex flex-col">
            {categories?.map((item, index) => (
              <Link
                href={`/Category/${item.code}`}
                key={index}
                className="cursor-pointer hover:font-medium hover:text-[#f51167] pt-[12px] pb-[5px] text-[12px] text-[#414141] border-b-[1px] border-[#00000020]"
              >
                {item.title}
              </Link>
            ))}
          </ul>
          <div className="mt-[40px] py-[30px] border-b-[1px] border-[#00000020]">
            <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
              refine by
            </h1>
            <div className="flex items-center gap-3">
              <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
                min price
              </h1>
              <div className="w-[140px] px-[15px] h-[40px] mb-[40px] rounded-[25px] border-[1px] border-[#00000020] flex items-center gap-2">
                <button onClick={handleValueMin} className="w-[20px]">
                  -
                </button>
                <input type="number" className="w-[72px]" value={valueMin} />
                <button onClick={handleValueMinIncrease} className="w-[20px]">
                  +
                </button>
              </div>
            </div>
            <div className="flex items-center gap-3">
              <h1 className="uppercase tex-[18px] font-bold text-[#414141] mb-[40px]">
                max price
              </h1>
              <div className="w-[140px] px-[15px] h-[40px] mb-[40px] rounded-[25px] border-[1px] border-[#00000020] flex items-center gap-2">
                {" "}
                <button type="button" onClick={handleValueMaxDecrease} className="w-[20px]">-</button>
                <input type="number" className="w-[72px]" value={valueMax} />
                <button type="button" onClick={handleValueMaxIncrease} className="w-[20px]">+</button>
              </div>
            </div>
            <button onClick={handleFilterProduct} className=" rounded-md overflow-hidden px-[67px] py-[14px] bg-[#f51167] text-white font-bold uppercase">
              filter
            </button>
          </div>
        </div>

        <div className="grid sm:grid-cols-2 md:grid-cols-3 gap-[30px]">
          {productCategory.map((item, index) => (
            <CardProduct
              id={item._id}
              key={index}
              img={item.image}
              desc={item.desc}
              price={item.price}
            />
          ))}
        </div>
        <div className="flex items-center">
          <button onClick={PrevPage} className="bg-red-500 text-white border-[1px] rounded-md border-[#00000020]">Prev</button>
          <button onClick={NextPage}>Next</button>
        </div>
      </div>
    </div>
  );
};

export default page;
