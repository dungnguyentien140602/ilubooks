
import React from "react";
import NavbarPurchase from "@/components/User/NavbarPurchase";
export default function Layout({ children }) {
  return (
    <div className="flex-1">
       <NavbarPurchase/>
       {children}
    </div>
  );
}
