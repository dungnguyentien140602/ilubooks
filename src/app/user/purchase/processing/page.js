"use client";
import { orderProcessing } from "@/actions/order";
import { getProductId } from "@/actions/userActions";
import { useRouter } from "next/navigation";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
const page = () => {
  const [dataAllProcessing, setdataAllProcessing] = useState([]);
  const userData = useSelector((state) => state.auth);
  const router = useRouter();


  useEffect(() => {
    orderProcessing(userData.user._id)
      .then((data) => {
        setdataAllProcessing(data);
      })
      .catch((err) => console.log(err.message));
  }, []);


  const getPriceProductNumber = useMemo(() => {
    return (number, value) => {
      return number * value;
    };
  }, []);

  function formatCurrency(num) {
    if (num) {
      return num.toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
      });
    }
    return null;
  }

  return (
    <div>
      {dataAllProcessing?.reverse().map((item, index) => (
        <div key={`pending_${index}`}>
          <div className="px-[25px] bg-white mb-[20px] shadow-md">
            <div className="flex justify-end pt-[20px] pb-[15px]">
              <h1 className="text-[18px] text-[#f51167]">
                Đơn hàng đang chờ vận chuyển
              </h1>
            </div>
            <div className="py-[15px] mb-[10px] border-t-[1px] border-b-[1px] border-[#00000020]">
              {item?.items.map((productOrder, subIndex) => (
                <div
                  key={`product_${index}_${subIndex}`}
                  className="mb-[20px] flex items-center"
                >
                  <div className="w-[85%] flex gap-3">
                    <img
                      className="w-[80px] h-[80px] object-cover"
                      src={`http://localhost:4000/${productOrder.product_id?.image}`}
                    ></img>
                    <div className="flex-1">
                      <h2>{productOrder.product_id?.title}</h2>
                      <p>
                        Phân loại hàng :
                        {productOrder.attributes?.map(
                          (itemAttr, subSubIndex) => (
                            <span
                              key={`attr_${index}_${subIndex}_${subSubIndex}`}
                            >
                              {itemAttr.value}
                            </span>
                          )
                        )}
                      </p>
                      <span>{productOrder.quantity}</span>
                    </div>
                  </div>
                  <div className="flex-1 flex items-center justify-end">
                    <span className="text-[#f51167]">
                    {formatCurrency(
                        getPriceProductNumber(
                          productOrder.quantity,
                          productOrder.product_id?.price
                        )
                      )}
                    </span>
                  </div>
                </div>
              ))}
            </div>
            <div className="py-[20px]">
              <div className="flex justify-end">
                <div className="flex items-center gap-3">
                  <span>Số tiền phải trả</span>
                  <h2 className="text-[24px] text-[#f51167]">
                    {formatCurrency(item.totalPrice)}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default page;
