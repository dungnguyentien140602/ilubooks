"use client";
import { orderCancelled, updateOrderByBody} from "@/actions/order";
import { useRouter } from "next/navigation";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
const page = () => {
  const [dataAllCancelled, setDataAllCancelled] = useState([]);
  const userData = useSelector((state) => state.auth);
  const router = useRouter();

  useEffect(() => {
    orderCancelled(userData?.user._id)
      .then((data) => {
        setDataAllCancelled(data);
      })
      .catch((err) => console.log(err.message));
  }, [userData]);


  const getPriceProductNumber = useMemo(() => {
    return (number, value) => {
      return number * value;
    };
  }, []);

  const formatCurrency = (num) => {
    if (num) {
      return num.toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
      });
    }
    return null;
  };

  let pending = 'pending'
  
  const handleRepurchase = (id_order) => {
    updateOrderByBody(id_order , pending )
      .then((data) => {
        router.push('/user/purchase/waitpay')
        toast.success("Tạo đơn hàng thành công")
      })
      .catch((err) => console.log(err.message));
  };

  return (
    <div>
      {dataAllCancelled.reverse().map((item, index) => (
    <div key={`cancelled_${index}`}>
      <div className="px-[20px] mb-[20px] w-ful bg-white shadow-sm">
        {item?.items.map((productOrder, subIndex) => (
          <div
            key={`product_${index}_${subIndex}`}
            className="flex items-center border-b-[1px] border-[#00000020] rounded-sm"
          >
            <div className="flex items-start w-[80%] py-[20px]  gap-3">
              <img
                src={`http://localhost:4000/${productOrder.product_id?.image}`}
                className="w-[85px] h-[85px]"
              ></img>
              <div>
                <h2>{productOrder.product_id?.title}</h2>
                <p>
                  Phân loại hàng :
                  {productOrder.attributes?.map(
                    (itemAttr, subSubIndex) => (
                      <span key={`attr_${index}_${subIndex}_${subSubIndex}`}>{itemAttr.value}</span>
                    )
                  )}
                </p>
                <span>{productOrder.quantity}</span>
              </div>
            </div>
            <div className="flex-1 flex justify-end">
              <span>{ productOrder.product_id?.price}</span>
            </div>
          </div>
        ))}
        <div className="flex justify-end py-[25px]">
          <div>
            <div className="flex gap-3 items-center">
              <p>Thành Tiền</p>
              <span className="text-[24px] text-[#f51167]">
                {" "}
                {formatCurrency(item.totalPrice)}
              </span>
            </div>
            <div className="justify-end flex">
              <div className="mt-[10px] flex items-center gap-2">
                <button
                  onClick={() => handleRepurchase(item._id)}
                  className="px-[50px] py-[10px] bg-[#f51168] text-white font-medium rounded-md"
                >
                  Mua lại
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
))}

    </div>
  );
};

export default page;
