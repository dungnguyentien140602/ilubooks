
import React from "react";
import Sidebar from "@/components/User/Sidebar";
export default function Layout({ children }) {
  return (
    <div className="bg-[#f5f5f5] min-h-[100vh]">
        <div className="xl:mx-[190px] pt-[20px] gap-4 flex items-start">
         <Sidebar />
         {children}
        </div>
    </div>
  );
}
