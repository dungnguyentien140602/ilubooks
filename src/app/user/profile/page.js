"use client";
import { updateInfo } from "@/actions/userActions";
import { DatePicker, Space } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
const page = () => {
  const userData = useSelector((state) => state.auth);
  const [date_of_birth, setDate] = useState("");
  const [gender, setGender] = useState(userData.user.gender);
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [image, setImage] = useState();
  const onChange = (date, dateString) => {
    console.log(dateString);
    setDate(dateString);
  };
  const dispatch = useDispatch();
  const handleUpdateInfo = (event) => {
    event.preventDefault();
    console.log(date_of_birth, gender, name, phone);
    const formData = new FormData();
    formData.append("name", name);
    formData.append("gender", gender);
    formData.append("date_of_birth", date_of_birth);
    formData.append("phone", phone);
    formData.append("image", image);

    updateInfo(formData)
      .then((data) => {
        toast.success("Update infor success", {
          autoClose: 1000,
        });
        dispatch({ type: "LOGIN_SUCCESS", payload: data });
      })
      .catch((err) => console.log(err.message));
  };
  const handleFile = (e) => {
    setImage(e.target.files[0]);
  };
  return (
    <form
      onSubmit={handleUpdateInfo}
      className="flex-1 px-[30px] shadow-md py-[20px] bg-white"
    >
      <div className="pb-[20px] border-b-[1px] w-full border-[#00000020]">
        <h1>Hồ sơ của tôi</h1>
        <p>Quản lý thông tin hồ sơ để bảo mật tài khoản</p>
      </div>
      <div className="flex items-start">
        <div className="w-[78%] p-[30px]">
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px]">Tên</label>
            <input
              onChange={(e) => setName(e.target.value)}
              name="name"
              placeholder={userData.user.name}
              value={name}
              className="px-[8px] rounded-md outline-none border-[1px] border-[#00000020] py-[6px] flex-1"
            ></input>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px]">Email</label>
            <span>{userData.user.email}</span>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px]">Giới tính</label>
            <div>
              <input
                onChange={(e) => setGender(e.target.value)}
                value="Nam"
                type="radio"
                checked={gender === "Nam"}
              ></input>{" "}
              Nam
              <input
                value="Nữ"
                onChange={(e) => setGender(e.target.value)}
                type="radio"
                checked={gender === "Nữ"}
              ></input>{" "}
              Nữ
            </div>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px]">Phone</label>
            <input
              type="text"
              name="phone"
              placeholder={userData.user.phone}
              value={phone}
              className="px-[8px] rounded-md outline-none border-[1px] border-[#00000020] py-[6px] flex-1"
              onChange={(e) => setPhone(e.target.value)}
            ></input>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px]">Ngày sinh</label>
            <div className="flex items-center gap-2">
              <Space direction="vertical">
                <DatePicker onChange={onChange} />
              </Space>
            </div>
          </div>
        </div>
        <div className="flex-1 flex flex-col items-center justify-center mt-[50px]">
          <img
            className="w-[102px] h-[102px] rounded-[50%]"
            src={`http://localhost:4000/${userData.user.image}`}
          ></img>
          <input type="file" onChange={handleFile}></input>
        </div>
      </div>
      <div className="flex items-center justify-center">
        <button
          type="submit"
          className="px-[20px] py-[8px] bg-[#f51167] text-white font-semibold rounded-md"
        >
          Lưu
        </button>
      </div>
    </form>
  );
};

export default page;
