import React from "react";

const page = () => {
  return (
    <div className="p-[30px] flex-1 shadow-md bg-white">
      <div className="pb-[30px] border-b-[1px] border-[#00000020]">
        <h1>Thêm mật khẩu</h1>
        <span>
          Để bảo mật tài khoản , vui lòng không chia sẻ mật khẩu cho bất kì ai{" "}
        </span>
      </div>
      <div className="pt-[30px]">
        <div className="ml-[100px] mr-[200px]">
          <div className="flex mb-[20px] items-center gap-3">
            <label className="w-[150px]">Mật khẩu mới</label>
            <input
              type="password"
              className="px-[8px] outline-none border-[1px] border-[#00000020] py-[6px] flex-1"
            ></input>
          </div>
          <div className="flex items-center gap-3">
            <label className="w-[150px]">Nhập lại mật khẩu</label>
            <input
              type="password"
              className="px-[8px] outline-none border-[1px] border-[#00000020] py-[6px] flex-1"
            ></input>
          </div>
        </div>
        <div className="flex items-center justify-center">
            <button className="px-[16px] py-[10px] text-white font-semibold bg-[#f51167] mt-[30px] rounded-md">Xác nhận</button>
        </div>
      </div>
    </div>
  );
};

export default page;
