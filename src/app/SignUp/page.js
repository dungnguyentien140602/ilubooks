'use client'
import Link from "next/link";
import React from "react";
import {useState} from 'react'
import { useRouter } from "next/navigation";
import { FaUser } from "react-icons/fa";
import {HiMail , HiKey} from "react-icons/hi";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
const page = () => {
  const router = useRouter();
  const [infoUser , setInfoUser] = useState(
    {
      name:"",
      email:"",
      password:""
    }
  );
  const handleSignUp = async (event) => {
     event.preventDefault();
     console.log(infoUser);
     try{
      const response = await axios.post('http://localhost:4000/auth/newUser' ,
       infoUser)
      console.log(response)
      if(response.data.status === "success"){
        toast.success('Register successfully , you need login' , {
          autoClose:1000
        },)
        router.push('SingIn')
      }
     }catch{

     }
  }
  const handleChangeData = (event) => {
    setInfoUser((infoUser) => (
      {
        ...infoUser , [event.target.name] : event.target.value
      }
     ))
  }
  return (
    <div className="xl:px-[175px] mx-auto py-[20px] w-full bg-white">
    <div className="flex rounded-xl border-none sm:border-[1px] overflow-hidden sm:border-[#00000020]">
    <div className="w-[45%] hidden lg:block h-full">
        <img
          src="https://seamaf.com/frontend/img/background.jpg"
          alt=""
          className="object-cover"
        />
      </div>
      {/* Form SignUp */}
      <form onSubmit={handleSignUp} className="flex-1 py-[32px] px-[48px] rounded-r-md rounded-b-md ">
        <h2 className="text-[24px] text-[#111111] font-[700] text-center mb-[20px]">
          Create an account
        </h2>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <FaUser />
          </span>
          <input
            onChange={(e)=> handleChangeData(e)}
            type="text"
            className="flex-1 h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Name"
            name="name"
          />
        </div>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <HiMail />
          </span>
          <input
            type="text"
            onChange={(e)=> handleChangeData(e)}
            name="email"
            className="flex-1 h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Email"
          />
        </div>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <HiKey />
          </span>
          <input
            type="password"
            onChange={(e)=> handleChangeData(e)}
            name="password"
            className="flex-1 h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Password"
          />
        </div>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <HiKey />
          </span>
          <input
            type="password"
            onChange={(e)=> handleChangeData(e)}
            name="confirmPassword"
            className="flex-1 h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Confirm Password"
          />
        </div>
        <button type="submit" className="w-full flex items-center py-[12px] justify-center mb-[16px] px-[12px] bg-[#F51167] border-[1px] gap-3 rounded-[5px] outline-none border-[#00000020]">
          <span className="text-white text-[15px] font-[700] text-center ">
            Sign Up
          </span>
        </button>
        <p className="text-center text-[16px] mb-[16px] text-[#212529]">
          or register with
        </p>
        <div className="flex items-center gap-[30px] pb-[25px] border-b-[1px] border-[#00000020]">
          <button className="w-[50%] px-[12px] py-[6px] rounded-md overflow-hidden bg-[#4866a8] text-[16px] font-[700] text-white">
            FaceBook
          </button>
          <button className="flex-1 px-[12px] py-[6px] bg-[#da3f34] rounded-md overflow-hidden text-[16px] font-[700] text-white">
            Google
          </button>
        </div>
        <div className="flex items-center justify-center pt-[25px] gap-2">
            <div className="flex items-center gap-2">
            <span className="text-[#212529] text-[16px] font-[500]">Already have an account?</span>
            <Link href='/SingIn' className="text-[#007BFF] font-[700] text-[16px]">Login Here</Link>
            </div>
        </div>
      </form>
    </div>
    </div>
  );
};

export default page;
