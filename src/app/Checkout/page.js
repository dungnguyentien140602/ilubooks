"use client";
import { orderConfirms, updateConfirm } from "@/actions/order";
import { useRouter } from "next/navigation";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { GiPositionMarker } from "react-icons/gi";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
const page = () => {
  const userData = useSelector((state) => state.auth);
  const [orderConfirmsData, setOrderConfirm] = useState([]);
  const [note, setNote] = useState("Not Note");
  const [shippingAddress, setShippingAddress] = useState("Hà Nội");
  const router = useRouter();
  useEffect(() => {
    orderConfirms(userData.user._id)
      .then((data) => {
        console.log(data);
        setOrderConfirm(data[data.length - 1]);
      })
      .catch((error) => console.log(error.message));
  }, []);

  function formatCurrency(num) {
    if (num) {
      return num.toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
      });
    }
    return null;
  }

  const getPriceProductNumber = useMemo(() => {
    return (number, value) => {
      return number * value;
    };
  }, []);

  let status = "pending";
  const handlePutOrder = useCallback(() => {
    console.log(note);
    updateConfirm(orderConfirmsData._id, note, shippingAddress, status)
      .then((data) => router.push("/user/purchase/waitpay"))
      .catch((err) => console.log(err));
    toast.success("Xác nhận đơn thành công", {
      autoClose: 1000,
    });
  }, [orderConfirmsData._id, note, shippingAddress]);

  return (
    <div className="bg-[#f5f5f5] min-h-[100vh]">
      {/*  */}
      <div className="xl:mx-[175px] h-full">
        <div className="mx-auto py-[20px]">
          <div className="px-[15px] bg-white mb-[20px] py-[30px]">
            <div className="gap-2 flex items-center">
              {" "}
              <GiPositionMarker />
              <h1 className="text-[20px] text-[#f51167]">Địa chỉ nhận hàng</h1>
            </div>
            <div className="flex items-center mt-2 gap-2">
              <h2 className="text-[18px]  font-bold">
                Nguyễn Tiến Dũng (+84) 866942653
              </h2>
              <span className="text-[18px] font-normal"></span>
              <input
                placeholder={orderConfirmsData?.shippingAddress}
                value={shippingAddress}
                onChange={(e) => setShippingAddress(e.target.value)}
              ></input>
            </div>
          </div>
          <div className="">
            <div className="px-[15px] bg-white pt-[30px] flex items-center">
              <div className="w-[60%]">
                <h1>Sản phẩm</h1>
              </div>
              <div className="flex-1 flex justify-between">
                <h2>Đơn giá</h2>
                <h2>Số lượng</h2>
                <h2>Thành tiền</h2>
              </div>
            </div>
            {/* Sản phẩm */}
            <div className="mb-[20px] bg-white">
              {orderConfirmsData.items?.map((itemOrder, index) => (
                <div key={index}>
                  <div className="px-[15px] flex items-center border-b-[1px] border-[#00000020] py-[30px]">
                    <div className="w-[60%] gap-2 flex items-center">
                      <img
                        className="w-[40px] h-[40px] object-cover"
                        src={`http://localhost:4000/${itemOrder?.product_id.image}`}
                      ></img>
                      <h2>{itemOrder?.product_id.title} </h2>
                    </div>
                    <div className="flex-1 flex items-center justify-between">
                      <h2>{itemOrder?.product_id.price}</h2>
                      <h2>{itemOrder?.quantity}</h2>
                      <h2>
                        {" "}
                        {formatCurrency(
                          getPriceProductNumber(
                            itemOrder?.product_id.price,
                            itemOrder?.quantity
                          )
                        )}
                      </h2>
                    </div>
                  </div>
                  <div className="px-[15px] flex items-center bg-[#fafdff] py-[20px] justify-end">
                    <div className="flex items-center gap-4">
                      <h2>Tổng số tiền (1 sản phẩm)</h2>
                      <h1 className="text-[24px] text-[#f51167]">
                        {formatCurrency(
                          getPriceProductNumber(
                            itemOrder?.product_id.price,
                            itemOrder?.quantity
                          )
                        )}
                      </h1>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="mt-[20px] bg-white">
            <div className="px-[15px] flex items-center justify-between py-[30px] border-b-[1px] border-[#00000020]">
              <h1 className="text-[20px]">Phương Thức Thanh Toán</h1>
              <div className="flex items-center gap-[30px]">
                <h2>Thanh toán khi nhận hàng</h2>
                <h1 className="text-blue-400 text-[18px]">THAY ĐỔI</h1>
              </div>
            </div>
            <div className="py-[30px] flex items-center px-[15px] justify-between border-b-[1px] border-[#00000020]">
              <div className="flex items-center gap-3">
                <h2>Lời nhắn</h2>
                <input
                  className="border-[1px] border-[#00000020] w-[340px] px-[12px] py-[6px]"
                  placeholder="Lưu ý cho người bán..."
                  onChange={(e) => setNote(e.target.value)}
                ></input>
              </div>
              <div className="flex flex-col gap-[20px]">
                <div className="flex items-center justify-between gap-[30px]">
                  <h1>Tổng tiền hàng</h1>
                  <span>59.000đ</span>
                </div>
                <div className="flex items-center justify-between gap-[30px]">
                  <h1>Phí vận chuyển</h1>
                  <span>59.000đ</span>
                </div>
                <div className="flex items-center justify-between gap-[20px]">
                  <h1>Tổng thanh toán</h1>
                  <span className="text-[#f51167] text-[24px]">
                    {formatCurrency(orderConfirmsData?.totalPrice)}
                  </span>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-between px-[15px] py-[20px]">
              <h1 className="text-[#0000008A]">
                Đơn hàng sẽ được xác nhận và gửi đi ngay khi bạn đặt hàng
              </h1>
              <button
                onClick={handlePutOrder}
                className="py-[8px] px-[16px] font-medium text-white bg-[#f51167] border-none outline-none rounded-md"
              >
                Đặt Hàng
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default page;
