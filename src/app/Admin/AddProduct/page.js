"use client";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { useDispatch , useSelector} from "react-redux";
import { createProduct, getAllCategory } from "@/actions/userActions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSearchParams } from "next/navigation";
const Attribute = [
  {
    name: "Màu sắc",
    idAtr: "MS",
    value: [
      {
        nameAtr: "Xanh",
        id: 1,
      },
      {
        nameAtr: "Đỏ",
        id: 2,
      },
      {
        nameAtr: "Tím",
        id: 3,
      },
      {
        nameAtr: "Vàng",
        id: 4,
      },
      {
        nameAtr: "Nâu",
        id: 5,
      },
    ],
  },
  {
    name: "Size",
    idAtr: "SIZE",
    value: [
      {
        nameAtr: "M",
        id: 1,
      },
      {
        nameAtr: "L",
        id: 2,
      },
      {
        nameAtr: "S",
        id: 3,
      },
      {
        nameAtr: "XL",
        id: 4,
      },
      {
        nameAtr: "XXL",
        id: 5,
      },
    ],
  },
];

const Page = () => {
  const [infoProduct, setInfoProduct] = useState({
    title: "",
    desc: "",
    category: "",
    price: "",
  });
  const [selectedValues, setSelectedValues] = useState({});
  const [allAttribute, setAllAttribute] = useState([]);
  const dispatch = useDispatch();
  const params = useSearchParams();
  const [imageS , setImage] = useState()
  const pageCurrent = params.get("page") || 1;
  const [dataCategory , setDataCategory] = useState([]);
  const token = localStorage.getItem('token')
  useEffect(() => {
    getAllCategory(pageCurrent)
    .then((data) => setDataCategory(data.docs))
    .catch((err) => console.log(err.message))
  } , [pageCurrent])

  const handleChangeData = (event) => {
    setInfoProduct((infoProduct) => ({
      ...infoProduct,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    console.log(token);
    console.log(allAttribute);
    const docAttribute = JSON.stringify(allAttribute)
    const formData = new FormData();
    formData.append('title' , infoProduct.title)
    formData.append('desc' , infoProduct.desc)
    formData.append('price' , infoProduct.price)
    formData.append('category' , infoProduct.category)
    formData.append('image' , imageS)
    formData.append('attribute', docAttribute)
    console.log(formData);
    
    axios
      .post(
        "http://localhost:4000/products/newProduct",
         formData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.status === "success") {
          toast.success('Create Product Success', {
            autoClose: 1000,
          });
        }
        if (res.data.status === false) {
          toast.error('Not Create Product', { autoClose: 1000 });
        }
      })
      .catch((err) => {
         console.log(err.message);
      });
  };

  function handleOptionChange(event, attributeName) {
    const selectedOptions = Array.from(event.target.selectedOptions);
    const selectedValues = selectedOptions.map((option) => option.value);
    setSelectedValues((prevState) => {
      return {
        ...prevState,
        [attributeName]: selectedValues,
      };
    });
    setAllAttribute((prevState) => {
      if (selectedValues.length === 0) {
        // Remove the object with the matching name property
        return prevState.filter((obj) => obj.name !== attributeName);
      } else {
        // Update the object with the matching name property
        return [
          ...prevState.filter((obj) => obj.name !== attributeName),
          {
            name: attributeName,
            value: selectedValues.map((value) => ({ nameAtr: value })),
          },
        ];
      }
    });
    console.log(selectedValues);
  }
  const handleFile = (e) => {
    setImage(e.target.files[0]);
  }

  return (
    <div className="px-[30px] w-full min-h-[100vh] py-[30px]">
      <div className="w-full pb-[10px] border-b-[1px] border-[#00000020]">
        <h1 className="text-[20px] font-bold">Add Product</h1>
      </div>
      <form  onSubmit={handleSubmitForm} className=" border-[#00000020] mt-[10px]">
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="text"
              placeholder="name..."
              onChange={(e) => handleChangeData(e)}
              name="title"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
          <input type="file" onChange={handleFile}></input>
        </div>
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <select
              name="category"
              onChange={(e) => handleChangeData(e)}
              className="w-full px-[24px] py-[8px] outline-none"
              id=""
            >
              <option value="">--Category---</option>
              {
                dataCategory?.map((item , index) => 
                 <option value={item.code} key={index}>{item.title}</option>
                )
              }
            </select>
          </div>
          <div className="flex-1 shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="number"
              placeholder="Price..."
              onChange={(e) => handleChangeData(e)}
              name="price"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
        </div>
        <div className="flex gap-3 w-full items-start mt-[20px]">
          <div className="w-[50%] shadow-md border-l-[1px] border-[#00000020] rounded-md overflow-hidden">
            <textarea
              placeholder="Desc"
              className="border-[1px] w-full outline-none rounded-md px-[24px] py-[8px] border-[#00000020]"
              name="desc"
              onChange={(e) => handleChangeData(e)}
              id=""
              cols={30}
              rows={10}
            ></textarea>
          </div>
          <div className="flex-1 rounded-md overflow-hidden">
          {Attribute.map((itemAtr, index) => (
            <div className="flex items-center mb-[15px]" key={index}>
              <select
                multiple
                placeholder="Attribute"
                className="px-[10px] w-full shadow-md rounded-md outline-none py-[5px] border-[1px] border-[#00000020]"
                name={itemAtr.name}
                onChange={(event) => handleOptionChange(event, itemAtr.name)}
              >
                {itemAtr.value.map((valueAtr) => (
                  <option key={valueAtr.id} value={valueAtr.nameAtr}>
                    {valueAtr.nameAtr}
                  </option>
                ))}
              </select>
            </div>
          ))}
          </div>
        </div>
        <button
            type="submit"
            className="float-right text-center rounded-md px-[8px] py-[6px] border-none outline-none bg-red-500 text-[16px] font-bold text-white"
          >
            Post Sản Phẩm
          </button>
      </form>
    </div>
  );
};

export default Page;
