import AdminNav from '@/components/AdminNav'
import React from 'react'
export default function Layout({ children }) {
    return (
      <div className='flex items-start bg-white'>
          <AdminNav/>
          {children}
      </div>
    )
  }