"use client";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createCategory } from "@/actions/userActions";
const Page = () => {
  const [InfoProduct, setInfoProduct] = useState({
    title: "",
    code: "",
  });
  const dispatch = useDispatch();

  const handleChangeData = (event) => {
    setInfoProduct((InfoProduct) => ({
      ...InfoProduct,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();
    dispatch(createCategory(InfoProduct.title, InfoProduct.code));
  };
  return (
    <div className=" px-[30px] w-full min-h-[100vh] py-[30px]">
      <div className="w-full pb-[10px] border-b-[1px] border-[#00000020]">
        <h1 className="text-[20px] font-bold">Add Category</h1>
      </div>
      <form
        onSubmit={handleSubmitForm}
        className=" border-[#00000020] mt-[10px]"
      >
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="text"
              placeholder="Title..."
              onChange={(e) => handleChangeData(e)}
              name="title"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
          <div className="flex-1 shadow-md border-[1px] border-[#00000020]  rounded-md overflow-hidden">
            <input
              type="text"
              placeholder="Code..."
              onChange={(e) => handleChangeData(e)}
              name="code"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
        </div>
        <button
          type="submit"
          className="mt-[40px] float-right text-center rounded-md px-[8px] py-[6px] border-none outline-none bg-red-500 text-[16px] font-bold text-white"
        >
          Post Category
        </button>
      </form>
    </div>
  );
};

export default Page;
