"use client";
import axios from "axios";
import { useParams, useRouter } from "next/navigation";
import React, { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Attribute = [
  {
    name: "Màu sắc",
    idAtr: "MS",
    value: [
      {
        nameAtr: "Xanh",
        id: 1,
      },
      {
        nameAtr: "Đỏ",
        id: 2,
      },
      {
        nameAtr: "Tím",
        id: 3,
      },
      {
        nameAtr: "Vàng",
        id: 4,
      },
      {
        nameAtr: "Nâu",
        id: 5,
      },
    ],
  },
  {
    name: "Size",
    idAtr: "SIZE",
    value: [
      {
        nameAtr: "M",
        id: 1,
      },
      {
        nameAtr: "L",
        id: 2,
      },
      {
        nameAtr: "S",
        id: 3,
      },
      {
        nameAtr: "XL",
        id: 4,
      },
      {
        nameAtr: "XXL",
        id: 5,
      },
    ],
  },
];

const Page = () => {
  const [infoProduct, setInfoProduct] = useState({
    title: "",
    desc: "",
    category: "",
    price: "",
  });
  const [selectedValues, setSelectedValues] = useState({});
  const [allAttribute, setAllAttribute] = useState([]);
  const [dataCategory, setDataCategory] = useState([]);
  const params = useParams();
  const router = useRouter();
  const [imageS, setImage] = useState();
  const handleChangeData = (event) => {
    setInfoProduct((infoProduct) => ({
      ...infoProduct,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    if (token) {
      console.log(token);
      console.log(allAttribute);
      if (infoProduct.title == "") {
        toast.error("Title không được để trống" , {
          autoClose:1000
        });
      } else if (infoProduct.desc == "") {
        toast.error("Desc không được để trống" , {
          autoClose:1000
        });
      } else if (infoProduct.price == "") {
        toast.error("Price không được để trống" , {
          autoClose:1000
        });
      } else if (infoProduct.category == "") {
        toast.error("Category không được để trống" , {
          autoClose:1000
        });
      } else if (allAttribute == []) {
        toast.error("Attribute không được để trống" , {
          autoClose:1000
        });
      } else {
        const docAttribute = JSON.stringify(allAttribute);
        const formData = new FormData();
        formData.append("title", infoProduct.title);
        formData.append("desc", infoProduct.desc);
        formData.append("price", infoProduct.price);
        formData.append("category", infoProduct.category);
        formData.append("image", imageS);
        formData.append("attribute", docAttribute);
        console.log(infoProduct);
        console.log(imageS);
        console.log(formData);
        console.log(docAttribute);
        axios
          .put(
            `http://localhost:4000/products/putProduct/${params.productId}`,
            formData,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((res) => {
            console.log(res.data);
            if (res.data.status === "success") {
              toast.success(res.data.msg, {
                autoClose: 1000,
                onClose: () => {
                  router.push("/Admin/ListProduct");
                },
              });
            }
          })
          .catch((err) => {
            console.log(err.message);
          });
      }
    }
  };

  const token = localStorage.getItem("token");

  useEffect(() => {
    axios
      .get("http://localhost:4000/category/getAll", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res?.data.allCategory);
        setDataCategory(res?.data.allCategory);
      });
  }, []);

  const handleFile = (e) => {
    console.log(e.target.files[0]);
    setImage(e.target.files[0]);
  };


  const handleOptionChange = useCallback(
    (event, attributeName) => {
      const selectedOptions = Array.from(event.target.selectedOptions);
      const selectedValues = selectedOptions.map((option) => option.value);

      setSelectedValues((prevState) => ({
        ...prevState,
        [attributeName]: selectedValues,
      }));

      setAllAttribute((prevState) => {
        if (selectedValues.length === 0) {
          // Remove the object with the matching name property
          return prevState.filter((obj) => obj.name !== attributeName);
        } else {
          // Update the object with the matching name property
          return [
            ...prevState.filter((obj) => obj.name !== attributeName),
            {
              name: attributeName,
              value: selectedValues.map((value) => ({ nameAtr: value })),
            },
          ];
        }
      });

      console.log(selectedValues);
    },
    []
  );

  return (
    <div className="px-[30px] w-full min-h-[100vh] py-[30px]">
      <div className="w-full pb-[10px] border-b-[1px] border-[#00000020]">
        <h1 className="text-[20px] font-bold">Edit Product</h1>
      </div>
      <form
        onSubmit={handleSubmitForm}
        className=" border-[#00000020] mt-[10px]"
      >
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="text"
              placeholder="name..."
              onChange={(e) => handleChangeData(e)}
              name="title"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
          <input type="file" onChange={handleFile}></input>
        </div>
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <select
              name="category"
              onChange={(e) => handleChangeData(e)}
              className="w-full px-[24px] py-[8px] outline-none"
              id=""
            >
              <option value="">--Category---</option>
              {dataCategory?.map((item, index) => (
                <option value={item.code} key={index}>
                  {item.title}
                </option>
              ))}
            </select>
          </div>
          <div className="flex-1 shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="number"
              placeholder="Price..."
              onChange={(e) => handleChangeData(e)}
              name="price"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
        </div>
        <div className="flex gap-3 w-full items-start mt-[20px]">
          <div className="w-[50%] shadow-md border-l-[1px] border-[#00000020] rounded-md overflow-hidden">
            <textarea
              placeholder="Desc"
              className="border-[1px] w-full outline-none rounded-md overflow-hidden px-[24px] py-[8px] border-[#00000020]"
              name="desc"
              onChange={(e) => handleChangeData(e)}
              id=""
              cols={30}
              rows={10}
            ></textarea>
          </div>
          <div className="flex-1 rounded-md overflow-hidden">
            {Attribute.map((itemAtr, index) => (
              <div
                className="flex shadow-md items-center mb-[15px]"
                key={index}
              >
                <select
                  multiple
                  placeholder="Attribute"
                  className="px-[10px] w-full outline-none rounded-md py-[5px] border-[1px] border-[#00000020]"
                  name={itemAtr.name}
                  onChange={(event) => handleOptionChange(event, itemAtr.name)}
                >
                  {itemAtr.value.map((valueAtr) => (
                    <option key={valueAtr.id} value={valueAtr.nameAtr}>
                      {valueAtr.nameAtr}
                    </option>
                  ))}
                </select>
              </div>
            ))}
          </div>
        </div>
        <button
          type="submit"
          className="float-right text-center rounded-md px-[8px] py-[6px] border-none outline-none bg-red-500 text-[16px] font-bold text-white"
        >
          Sửa Sản Phẩm
        </button>
      </form>
    </div>
  );
};

export default Page;
