import React from 'react'

const page = () => {
  return (
    <div className="container mx-auto my-5">
      <h1 className="text-xl ml-[30px] font-bold mb-3">Đơn hàng</h1>
      <table className="border-collapse border border-gray-400 w-full">
        <thead>
          <tr className="bg-gray-200 text-gray-700 uppercase text-sm">
            <th className="py-2 px-3 text-left">User</th>
            <th className="py-2 px-3 text-left">Items</th>
            <th className="py-2 px-3 text-left">Note</th>
            <th className='py-2 px-3 text-left'>Status</th>
            <th className='py-2 px-3 text-left'>Total Price</th>
            <th className='py-2 px-3 text-left'>Shipping Address</th>
            <th className='py-2 px-3 text-left'>Options</th>
          </tr>
        </thead>
        <tbody>
          <tr className="hover:bg-gray-100">
            <td className="py-2 px-3 border-t border-gray-400">Nguyễn Tiến Dũng</td>
            <td className="py-2 px-3 border-t border-gray-400">Sản phẩm chưa có</td>
            <td className="py-2 px-3 border-t border-gray-400">Bút bi mới</td>
            <td className="py-2 px-3 border-t border-gray-400"><span className="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-slate-500 rounded-full">cancelled</span></td>
            <td className="py-2 px-3 border-t border-gray-400">1,000,000 VND</td>
            <td className="py-2 px-3 border-t border-gray-400">Hà Nội, Jakarta</td>
            <td className='py-2 px-3 border-t border-gray-400'>
              <button className='px-2 py-2 bg-red-500 text-white rounded-md'>Cập nhật</button>
              <button className='ml-2 px-2 py-2 bg-blue-200 text-white rounded-md'>Xóa</button>
            </td>
          </tr>
          <tr className="hover:bg-gray-100">
            <td className="py-2 px-3 border-t border-gray-400">Nguyễn Tiến Dũng</td>
            <td className="py-2 px-3 border-t border-gray-400">Sản phẩm chưa có</td>
            <td className="py-2 px-3 border-t border-gray-400">Bút bi mới</td>
            <td className="py-2 px-3 border-t border-gray-400"><span className="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-slate-500 rounded-full">cancelled</span></td>
            <td className="py-2 px-3 border-t border-gray-400">1,000,000 VND</td>
            <td className="py-2 px-3 border-t border-gray-400">Hà Nội, Jakarta</td>
            <td className='py-2 px-3 border-t border-gray-400'>
              <button className='px-2 py-2 bg-red-500 text-white rounded-md'>Cập nhật</button>
              <button className='ml-2 px-2 py-2 bg-blue-200 text-white rounded-md'>Xóa</button>
            </td>
          </tr>
          <tr className="hover:bg-gray-100">
            <td className="py-2 px-3 border-t border-gray-400">Nguyễn Tiến Dũng</td>
            <td className="py-2 px-3 border-t border-gray-400">Sản phẩm chưa có</td>
            <td className="py-2 px-3 border-t border-gray-400">Bút bi mới</td>
            <td className="py-2 px-3 border-t border-gray-400"><span className="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-slate-500 rounded-full">cancelled</span></td>
            <td className="py-2 px-3 border-t border-gray-400">1,000,000 VND</td>
            <td className="py-2 px-3 border-t border-gray-400">Hà Nội, Jakarta</td>
            <td className='py-2 px-3 border-t border-gray-400'>
              <button className='px-2 py-2 bg-red-500 text-white rounded-md'>Cập nhật</button>
              <button className='ml-2 px-2 py-2 bg-blue-200 text-white rounded-md'>Xóa</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default page
