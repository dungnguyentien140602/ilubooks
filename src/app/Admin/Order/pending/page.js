"use client";
import {
  allOrderAdminView,
  deleteOrderByID,
  updateOrderByBody,
} from "@/actions/order";
import { getProductId, getUserByUserId } from "@/actions/userActions";
import React from "react";
import { useMemo } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useCallback } from "react";
import { AiFillEdit } from "react-icons/ai";
import { GrCircleInformation } from "react-icons/gr";
import { IoEyeSharp } from "react-icons/io5";
import { MdDelete } from "react-icons/md";
import { toast } from "react-toastify";
import { RiDeleteBin6Line } from "react-icons/ri";

const formatCurrency = (num) => {
  if (num) {
    return num.toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
    });
  }
  return null;
};

const TimeConverter = (time) => {
  const date = new Date(time).toLocaleString();
  return date;
};

const page = () => {
  const [detailOrder, setDetailOrder] = useState({});
  const [dataAllPendings, setDataAllPendings] = useState([]);
  const [selectStatus, setSelectStatus] = useState("");

  useEffect(() => {
    allOrderAdminView()
      .then((data) => {
        console.log(data);
        setDataAllPendings(data);
      })
      .catch((err) => console.log(err.message));
  }, []);

  const getPriceProductNumber = useMemo(() => {
    return (number, value) => {
      return number * value;
    };
  }, []);

  const handleDetailOrder = async (data) => {
    window.my_modal_4.showModal();

    try {
      const userData = await getUserByUserId(data.user);
      console.log(userData);
      console.log(data.user);
      const mergedData = { ...data, ...userData };
      console.log(mergedData);
      setDetailOrder(mergedData);
    } catch (error) {
      console.log(error.message);
    }
  };

  // Update Status Order
  const handleSubmitStatus = async () => {
    if (!selectStatus) {
      setSelectStatus(detailOrder.status);
    }
    try {
      const updatedData = await updateOrderByBody(
        detailOrder._id,
        selectStatus
      );
      setDataAllPendings(updatedData);
      toast.success("Cập nhật trạng thái thành công");
    } catch (error) {
      console.log(error.message);
    }
  };

  const handleShowEdit = (item) => {
    setDetailOrder(item);
    window.my_modal_1.showModal();
  };
  // Delete Order
  const handleDeleteOrder = (id) => {
    deleteOrderByID(id)
      .then((data) => setDataAllPendings(data))
      .catch((error) => console.log(error.message));
  };

  return (
    <div className="container mx-auto my-5">
      <h1 className="text-xl ml-[30px] font-bold mb-3">Đơn hàng</h1>
      <table className="border-collapse border border-gray-400 w-full">
        <thead>
          <tr className="bg-gray-200 text-gray-700 uppercase text-sm">
            <th className="py-2 px-3 text-left font-normal">
              <input type="checkbox"></input>
            </th>
            <th className="py-2 px-3 text-left font-normal">Code Order</th>
            <th className="py-2 px-3 text-left font-normal">User</th>
            <th className="py-2 px-3 text-left font-normal">Note</th>
            <th className="py-2 px-3 text-left font-normal">Status</th>
            <th className="py-2 px-3 text-left font-normal">Total Price</th>
            <th className="py-2 px-3 text-left font-normal">
              Shipping Address
            </th>
            <th className="py-2 px-3 text-left font-normal">Date Created</th>
            <th className="py-2 px-3 text-left font-normal">Options</th>
          </tr>
        </thead>
        <tbody>
          {dataAllPendings?.map((item, index) => (
            <tr
              key={index}
              className="cursor-pointer hover:bg-gray-100 h-[50px] overflow-y-auto"
            >
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                <input type="checkbox"></input>
              </td>
              <td className="py-3 px-3 text-[14px] border-t text-blue-400 border-gray-400">
                {item._id}
              </td>
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                {item.user}
              </td>
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                {item.note}
              </td>
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                <span
                  className={`inline-flex items-center justify-center px-3 py-2 text-xs font-bold leading-none rounded-full ${
                    item.status === "cancelled"
                      ? "text-white bg-red-500"
                      : item.status === "pending"
                      ? "text-white bg-yellow-300"
                      : item.status === "delivered"
                      ? "text-white bg-green-300"
                      : item.status === "processing"
                      ? "text-white bg-pink-400"
                      : ""
                  }`}
                >
                  {item.status}
                </span>
              </td>

              <td className="py-3 px-3 text-[14px] border-t border-gray-400 font-semibold">
                {formatCurrency(item.totalPrice)}
              </td>
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                {item.shippingAddress}
              </td>
              <td className="py-3 px-3 text-[14px] border-t border-gray-400">
                {TimeConverter(item.createdAt)}
              </td>
              <td className="py-3 px-3 flex items-center gap-2 text-[14px] border-t border-gray-400">
                <button
                  className="text-[22px] text-blue-500  rounded-md"
                  onClick={() => handleDetailOrder(item)}
                >
                  <IoEyeSharp />
                </button>

                <button
                  className="ml-2 text-[22px]  text-orange-500 rounded-md"
                  onClick={() => handleShowEdit(item)}
                >
                  <AiFillEdit />
                </button>
                <button
                  className="text-[22px] text-red-800"
                  onClick={() => handleDeleteOrder(item._id)}
                >
                  <RiDeleteBin6Line />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <dialog id="my_modal_4" className="modal">
        <form
          method="dialog"
          className="modal-box bg-slate-100 w-11/12 max-w-5xl"
        >
          <div className="bg-white rounded-lg p-[20px]">
            <h3 className="font-bold text-lg">Order</h3>
            <div className="flex gap-3 items-center justify-between my-[20px]">
              <h2 className="w-[30%] font-bold">Chi tiết chung</h2>
              <h2 className="flex-1 font-bold">Thông tin thanh toán</h2>
              <h2 className="w-[35%] font-bold">Chi tiết giao nhận</h2>
            </div>
            <div className="flex gap-3 items-center justify-between">
              <div className="flex flex-col w-[30%] gap-1">
                <p>
                  Thời gian đặt hàng:{" "}
                  <span className="font-light">
                    {" "}
                    {TimeConverter(detailOrder?.createdAt)}
                  </span>
                </p>
                <p>
                  Tình trạng đơn hàng:{" "}
                  <span className="text-red-500">Đang xử lý</span>
                </p>
                <p>
                  Tổng giá :{" "}
                  <span className="text-[#f51167] text-[20px]">
                    {detailOrder?.totalPrice}
                  </span>
                </p>
              </div>
              <div className="flex flex-col flex-1 gap-1">
                <p>Địa chỉ : Hồ Đền Lừ 3</p>
                <p>
                  <p>
                    Email :{" "}
                    <span className="text-blue-400">{detailOrder?.email}</span>
                  </p>
                </p>
                <p>
                  Số điện thoại : <span>0866942653</span>
                </p>
              </div>
              <div className="flex flex-col w-[35%] gap-1">
                <p>Tên : {detailOrder?.name}</p>
                <p>Địa chỉ :{detailOrder?.shippingAddress}</p>
                <p>
                  Email :{" "}
                  <span className="text-blue-400">{detailOrder?.email}</span>
                </p>
                <p>
                  Ghi chú : <span>{detailOrder?.note}</span>
                </p>
              </div>
            </div>
          </div>
          <div className="bg-white rounded-md p-[20px] mt-[20px] max-h-[120px] overflow-y-auto">
            <h1 className="font-semibold mb-[20px]">Sản phẩm</h1>
            {detailOrder?.items?.map((itemProduct, index) => (
              <div key={index} className="flex items-center gap-3 mb-[15px]">
                <img
                  src={`http://localhost:4000/${itemProduct.product_id.image}`}
                  className="w-[50px] h-[50px] object-cover"
                ></img>
                <div>
                  <h1 className="text-[16px]">
                    {itemProduct.product_id.title}
                  </h1>
                </div>
              </div>
            ))}
          </div>
          <div className="modal-action">
            {/* if there is a button, it will close the modal */}
            <button className="btn">Close</button>
          </div>
        </form>
      </dialog>
      <dialog id="my_modal_1" className="modal">
        <form method="dialog" className="modal-box">
          <h3 className="font-bold text-lg">Hello!</h3>
          <div className="flex items-center justify-center">
            <div className="flex items-center gap-5">
              <h1>Status</h1>
              <select
                onChange={(e) => setSelectStatus(e.target.value)}
                className="px-[16px] py-[12px] border-[1px] border-[#00000020] rounded-md"
              >
                <option
                  selected={detailOrder.status === "cancelled"}
                  value="cancelled"
                >
                  Cancelled
                </option>
                <option
                  selected={detailOrder.status === "pending"}
                  value="pending"
                >
                  Pending
                </option>
                <option
                  selected={detailOrder.status === "processing"}
                  value="processing"
                >
                  Processing
                </option>
                <option
                  selected={detailOrder.status === "delivered"}
                  value="delivered"
                >
                  Delivered
                </option>
              </select>
            </div>
          </div>

          <div className="modal-action">
            {/* if there is a button in form, it will close the modal */}
            <button
              onClick={handleSubmitStatus}
              className="px-[8px] py-[6px] rounded-md bg-[#f51167] text-white"
            >
              Update
            </button>
            <button type="btn">Close</button>
          </div>
        </form>
      </dialog>
    </div>
  );
};

export default page;
