"use client";
import { getAllUsers, updateRole } from "@/actions/userActions";
import Link from "next/link";
import { useSearchParams } from "next/navigation";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { IoEyeSharp } from "react-icons/io5";
import { AiFillEdit } from "react-icons/ai";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";

const page = () => {
  const [data, setData] = useState([]);
  const params = useSearchParams();
  const [detailUser, setDetailUser] = useState({});
  const dispatch = useDispatch();
  const router = useRouter();
  const pageCurrent = params.get("page") || 1;
  const [role, setSelectRole] = useState("user");
  useEffect(() => {
    getAllUsers(pageCurrent)
      .then((data) => setData(data))
      .catch((err) => console.log(err.message));
  }, [pageCurrent]);
  function formatDateTime(dateTimeStr) {
    const date = new Date(dateTimeStr);
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZoneName: "short",
    };
    return date.toLocaleDateString("en-US", options);
  }
  const handlePrevPagi = () => {
    if (pageCurrent <= 1) {
      console.log(pageCurrent);
    } else {
      router.push(`/Admin/ListUsers?page=${parseInt(pageCurrent) + 1}`);
    }
  };
  const handleNextPagi = () => {
    if (pageCurrent == data.totalPages) {
      console.log("Trang cuoi");
    } else {
      router.push(`/Admin/ListUsers?page=${parseInt(pageCurrent) + 1}`);
    }
  };
  const handleShowEdit = (item) => {
    console.log(item);
    setDetailUser(item);
    window.my_modal_4.showModal();
  };
  const handleUpdateRole = () => {
    updateRole(detailUser._id, role, pageCurrent)
      .then((data) => {
        toast.success("Update role success");
        setData(data.allUsersUpdate);
      })
      .catch((error) => console.log(error.message));
  };
  return (
    <div className="w-full overflow-x-auto">
      <div className="flex justify-between p-[20px]">
        <h1 className="text-[20px] font-bold">List User</h1>
      </div>
      <table className="table w-full">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>CreatedAt</th>
            <th>UpdatedAt</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {data?.docs?.map((item) => (
            <tr
              key={item.id}
              className="border-b border-gray-200 hover:bg-gray-100 cursor-pointer"
            >
              <td className="px-2 text-[14px] py-3">{item._id}</td>
              <td className="px-2 text-[14px] py-3">{item.name}</td>
              <td className="px-2 text-[14px] py-3">{item.email}</td>
              <td className="px-2 text-[14px] py-3">
                <span
                  className={`inline-flex items-center justify-center w-[60px] py-2 text-xs font-bold leading-none rounded-full ${
                    item.role === "Admin"
                      ? "text-white bg-red-500"
                      : item.role === "user"
                      ? "text-white bg-yellow-300"
                      : ""
                  }`}
                >
                  {item.role}
                </span>
              </td>
              <td className="px-2 text-[14px] py-3">
                {formatDateTime(item.createdAt)}
              </td>
              <td className="px-2 text-[14px] py-3">
                {formatDateTime(item.updatedAt)}
              </td>
              <td
                className="px-4 text-[14px] flex items-center gap-3"
                onClick={() => handleShowEdit(item)}
              >
                <button className="ml-2 text-[22px]  text-orange-500 rounded-md">
                  <AiFillEdit />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="flex justify-end mr-[40px] h-[30px] my-[30px]">
        <div className='flex items-center gap-1'>
          <button onClick={handlePrevPagi} className="border-[1px] border-[#00000020] px-3 rounded-md py-2">
            «
          </button>
          {Array.from(Array(data.totalPages), (e, i) => (
            <Link key={i} href={`/Admin/ListUsers?page=${i + 1}`}>
              <button className={`${pageCurrent == i + 1 ? 'flex items-center justify-center border-[1px] border-[#00000020] text-white px-3 rounded-md py-2 font-bold bg-[#f51167]' : 'flex items-center justify-center border-[1px] border-[#00000020] px-3 rounded-md py-2'}`}>
                {i + 1}
              </button>
            </Link>
          ))}
          <button onClick={handleNextPagi} className="border-[1px] border-[#00000020] px-3 rounded-md py-2">
            »
          </button>
        </div>
      </div>
      

      <dialog id="my_modal_4" className="modal">
        <form method="dialog" className="modal-box w-4/12 max-w-xl">
          <h3 className="font-bold text-lg mb-[20px]">User!</h3>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Tên đăng nhập</label>
            <p>{detailUser.name}</p>
          </div>

          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Email</label>
            <p>{detailUser.email}</p>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Giới tính</label>
            <div>
              <input type="radio"></input> Nam
            </div>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Phone</label>
            <span>0866942653</span>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Ngày sinh</label>
            <p>14/06/2002</p>
          </div>
          <div className="flex items-center mb-[30px] gap-3">
            <label className="w-[120px] font-light">Role</label>
            <div className="flex items-center gap-2">
              <select
                onChange={(e) => setSelectRole(e.target.value)}
                className="px-[10px] border-[1px] border-[#00000020] rounded-md py-[8px]"
              >
                <option value="user" selected={detailUser.role === "user"}>
                  User
                </option>
                <option value="Admin" selected={detailUser.role === "Admin"}>
                  Admin
                </option>
              </select>
            </div>
          </div>
          <div className="modal-action">
            {/* if there is a button in form, it will close the modal */}
            <button
              type="btn"
              onClick={handleUpdateRole}
              className="px-2 py-2 bg-blue-500 text-white font-medium rounded-md"
            >
              Update
            </button>
            <button
              type="btn"
              className="bg-red-500 text-white font-medium px-2 py-2 rounded-md"
            >
              Close
            </button>
          </div>
        </form>
      </dialog>
    </div>
  );
};

export default page;
