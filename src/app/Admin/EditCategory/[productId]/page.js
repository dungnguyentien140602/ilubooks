"use client";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { createCategory, editCategory, getCategoryId } from "@/actions/userActions";
import { useParams } from "next/navigation";
const Page = () => {
  const [infoProduct, setInfoProduct] = useState({
    title: "",
    code: "",
  });
  const [data , setData] = useState();
  const params = useParams();
  console.log(params.productId);
  const dispatch = useDispatch();

  const handleChangeData = (event) => {
    setInfoProduct((infoProduct) => ({
      ...infoProduct,
      [event.target.name]: event.target.value,
    }));
  };
  useEffect(() => {
    getCategoryId(params.productId)
    .then((data) => {
        console.log(data);
        setData(data)
    })
    .catch((err) => console.log(err.message))
  } , [])
  

  const handleSubmitForm = async (event) => {
    event.preventDefault();
    dispatch(editCategory(params.productId , infoProduct.title , infoProduct.code ))
  };

  return (
    <div className="px-[30px] w-full min-h-[100vh] py-[30px]">
      <div className="w-full pb-[10px] border-b-[1px] border-[#00000020]">
        <h1 className="text-[20px] font-bold">Edit Category</h1>
      </div>
      <form  onSubmit={handleSubmitForm} className=" border-[#00000020] mt-[10px]">
        <div className="flex gap-3 w-full items-center mt-[20px]">
          <div className="w-[50%] shadow-md border-[1px] border-[#00000020] rounded-md overflow-hidden">
            <input
              type="text"
              placeholder={data?.title}
              onChange={(e) => handleChangeData(e)}
              name="title"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
          <div className="flex-1 shadow-md border-[1px] border-[#00000020]  rounded-md overflow-hidden">
            <input
              type="text"
              placeholder={data?.code}
              onChange={(e) => handleChangeData(e)}
              name="code"
              className="w-full outline-none px-[24px] py-[8px]"
            />
          </div>
        </div>
        <button
            type="submit"
            className="mt-[40px] float-right text-center rounded-md px-[8px] py-[6px] border-none outline-none bg-red-500 text-[16px] font-bold text-white"
          >
            Post Category
          </button>
      </form>
    </div>
  );
};

export default Page;
