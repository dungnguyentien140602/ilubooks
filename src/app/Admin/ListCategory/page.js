"use client"
import axios from 'axios';
import Link from 'next/link';
import React, { useEffect, useState, useCallback } from 'react'
import { useRouter, useSearchParams } from 'next/navigation';
import { deleteCategoryId, getAllCategory } from '@/actions/userActions';
import { useDispatch  , useSelector } from 'react-redux';

const Page = () => {
  const [data , setData] = useState([]);
  const token = localStorage.getItem('token')
  const router = useRouter();
  const params = useSearchParams();
  const pageCurrent = params.get("page") || 1;
  useEffect(() => {
    getAllCategory(pageCurrent)
    .then((data) => setData(data))
    .catch((err) => console.log(err.message))
  } , [pageCurrent])

  const handleEdit = useCallback((id) => {
    router.push(`Admin/EditCategory/${id}`)
  }, [router]);

  const handleDelete = useCallback((id) => {
    console.log(id);
    if(id) {
      deleteCategoryId(id , pageCurrent)
      .then((data) => setData(data.categorys))
      .catch((err) => console.log(err.message))
    }
  } , [setData])

  const handlePrevPagi = () => {
    if (pageCurrent <= 1) {
      console.log(pageCurrent);
    } else {
      router.push(`/Admin/ListCategory?page=${parseInt(pageCurrent) + 1}`);
    }
  };
  const handleNextPagi = () => {
    router.push(`/Admin/ListCategory?page=${parseInt(pageCurrent) + 1}`);
  };

  function TimeConverter(time) {
    const date = new Date(time).toLocaleString();
    return date
  }

  return (
    <div className="shadow-md px-[30px] w-full min-h-[100vh] py-[30px]">
      <div className='flex justify-between'>
      <h1 className='text-[20px] font-bold'>Category List</h1>
      <Link href='/Admin/AddProduct' className='cursor-pointer px-[12px] py-[4px] border-[1px] border-[#00000020] bg-[#f51167] text-white font-bold text-[14px] rounded-md'>Add Category</Link>
      </div>
      <table className="table-auto bg-white mt-4 border-collapse w-full">
        <thead>
          <tr>
            <th><input type='checkbox'></input></th>
            <th className="px-4 text-[14px] w-[20%] py-1 text-gray-600 font-bold">Title</th>
            <th className="px-4 text-[14px] w-[20%] py-1 text-gray-600 font-bold">Code</th>
            <th className="px-4 text-[14px] w-[20%] py-1 text-gray-600 font-bold">Created AT</th>
            <th className="px-4 text-[14px] w-[20%] py-1 text-gray-600 font-bold">Update AT</th>
            <th className="px-4 text-[14px] w-[20%] py-1 text-gray-600 font-bold">Options</th>
          </tr>
        </thead>
        <tbody>
          {data.docs?.map((item , index) => (
            <tr key={index} className="border-b border-gray-200 hover:bg-gray-100 cursor-pointer">
              <td><input type='checkbox'></input></td>
              <td className="px-4 text-[14px] py-1">{item.title}</td>
              <td className="px-4 text-[14px] py-1">{item.code}</td>
              <td className='px-4 text-[14px] py-1'>{TimeConverter(item.createdAt)}</td>
              <td className="px-4 text-[14px] py-1">{TimeConverter(item.updatedAt)}</td>
              <td className='px-4 text-[14px] gap-3 py-1 flex items-center'><button onClick={() => handleEdit(item._id)} className='px-[5px] py-[4px] text-white bg-red-500 rounded-md'>Sửa</button><button onClick={() => handleDelete(item._id)} className='px-[5px] py-[4px] text-white bg-green-500 rounded-md'>Xóa</button></td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="flex justify-end mr-[40px] h-[30px] my-[30px]">
        <div className='flex items-center gap-1'>
          <button onClick={handlePrevPagi} className="border-[1px] border-[#00000020] px-3 rounded-md py-2">
            «
          </button>
          {Array.from(Array(data.totalPages), (e, i) => (
            <Link key={i} href={`/Admin/ListCategory?page=${i + 1}`}>
              <button className={`${pageCurrent == i + 1 ? 'flex items-center justify-center border-[1px] border-[#00000020] text-white px-3 rounded-md py-2 font-bold bg-[#f51167]' : 'flex items-center justify-center border-[1px] border-[#00000020] px-3 rounded-md py-2'}`}>
                {i + 1}
              </button>
            </Link>
          ))}
          <button onClick={handleNextPagi} className="border-[1px] border-[#00000020] px-3 rounded-md py-2">
            »
          </button>
        </div>
      </div>
    </div>
  )
}

export default Page
