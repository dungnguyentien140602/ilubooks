"use client";
import { getAllProducts } from "@/actions/userActions";
import axios from "axios";
import Link from "next/link";
import { useRouter, useSearchParams } from "next/navigation";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { deleteProductById } from "@/actions/userActions";
const page = () => {
  const router = useRouter();
  const [data, setData] = useState([]);
  const [totalPages, setTotalPages] = useState();
  const params = useSearchParams();
  const pageCurrent = params.get("page") || 1;

  const handleEditProduct = useCallback(
    (item) => {
      console.log(item);
      router.push(`/Admin/EditProduct/${item._id}`);
    },
    [router]
  );

  const handleDelete = useCallback(
    (id) => {
      if (id) {
        deleteProductById(id, pageCurrent)
          .then((data) => {
            setData(data.products.docs);
          })
          .catch((error) => console.log(error.message));
      }
    },
    [setData, pageCurrent]
  );
  const fetchData = async () => {
    try {
      const data = await getAllProducts(pageCurrent);
      console.log(data.docs);
      console.log(data.totalPages);
      setTotalPages(data.totalPages);
      console.log(data.page);
      setData(data.docs);
    } catch (err) {
      console.log(err.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, [pageCurrent]);

  const handlePrevPagi = () => {
    if (pageCurrent <= 1) {
      console.log(pageCurrent);
    } else {
      router.push(`/Admin/ListProduct?page=${parseInt(pageCurrent) - 1}`);
    }
  };

  const handleNextPagi = () => {
    if(pageCurrent === data.totalPages){
      console.log(pageCurrent);
    }else{
      router.push(`/Admin/ListProduct?page=${parseInt(pageCurrent) + 1}`);
    }
  };

  return (
    <div className="w-full overflow-x-auto">
      <div className="flex justify-between p-[20px]">
        <h1 className="text-[20px] font-bold">Product List</h1>
        <button className="rounded-md px-[12px] py-[4px] text-white bg-red-400">
          Add Product
        </button>
      </div>
      <table className="table w-full">
        <thead>
          <tr>
            <th>Check</th>
            <th>Name</th>
            <th>Desc</th>
            <th>Image</th>
            <th>Price</th>
            <th>Category</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {data?.map((item) => (
            <tr
              key={item.id}
              className="border-b border-gray-200 hover:bg-gray-100 cursor-pointer"
            >
              <td>
                <input type="checkbox"></input>
              </td>
              <td className="px-4 text-[14px] py-1">{item.title}</td>
              <td className="px-4 text-[14px] py-1">{item.desc}</td>
              <td className="px-4 text-[14px] py-1">
                <img
                  className="w-[80px] h-[80px] object-cover"
                  src={`http://localhost:4000/${item.image}`}
                  alt={item.name}
                />
              </td>
              <td className="px-4 text-[14px] py-1">{item.price}</td>
              <td className="px-4 text-[14px] py-1">{item.category}</td>
              <td className="px-4 flex items-center justify-center text-[14px] gap-1 py-1">
                <button
                  onClick={() => handleEditProduct(item)}
                  className="mr-[10px] px-[5px] mt-[20px] py-[4px] text-white bg-red-500 rounded-md"
                >
                  Sửa
                </button>
                <button
                  onClick={() => handleDelete(item._id)}
                  className="px-[5px] py-[4px] mt-[20px] text-white bg-green-500 rounded-md"
                >
                  Xóa
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="flex justify-end mr-[40px] h-[30px] my-[30px]">
        <div className="flex items-center gap-1">
          <button
            onClick={handlePrevPagi}
            className="border-[1px] border-[#00000020] px-3 rounded-md py-2"
          >
            «
          </button>
          {Array.from(Array(totalPages), (e, i) => (
            <Link key={i} href={`/Admin/ListProduct?page=${i + 1}`}>
              <button
                className={`${
                  pageCurrent == i + 1
                    ? "flex items-center justify-center border-[1px] border-[#00000020] text-white px-3 rounded-md py-2 font-bold bg-[#f51167]"
                    : "flex items-center justify-center border-[1px] border-[#00000020] px-3 rounded-md py-2"
                }`}
              >
                {i + 1}
              </button>
            </Link>
          ))}
          <button
            onClick={handleNextPagi}
            className="border-[1px] border-[#00000020] px-3 rounded-md py-2"
          >
            »
          </button>
        </div>
      </div>
    </div>
  );
};

export default page;
