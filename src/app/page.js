"use client";
import Advisements from "@/components/Advisements";
import Feature from "@/components/Feature";
import { useRouter } from "next/navigation";
// import required modules
import { Autoplay, Navigation, Pagination } from "swiper";
import CardProduct from "@/components/CardProduct";
import { Swiper, SwiperSlide } from "swiper/react";
import { useState, useEffect } from "react";
import axios from "axios";
import CardSales from "@/components/CardSales";
import Header from "@/components/Header";
import Footer from "@/components/Footer";
import { getAllProducts } from "@/actions/userActions";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules

export default function Home() {
  const router = useRouter();
  const productData = [
    {
      categoryId: 1,
      titleCategory: "Latest Product",
      data: [
        {
          id: 1,
          img: "https://seamaf.com/storage/products/612/ZrVrSxtEGoW2mV.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
        {
          id: 2,
          img: "https://seamaf.com/storage/products/616/ojxRMmDnR7LV5K.webp",
          desc: "ring basket ball",
          price: "$2.1",
        },
        {
          id: 3,
          img: "https://seamaf.com/storage/products/614/QvOq4G5hkxvHv9.webp",
          desc: "plane white rentangle gift box (sile l )",
          price: "$2.3",
        },
        {
          id: 4,
          img: "https://seamaf.com/storage/products/613/GcRe2ZirwM8158.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
      ],
    },
    {
      categoryId: 2,
      titleCategory: "BROWSE TOP SELLING PRODUCTS",
      data: [
        {
          id: 5,
          img: "https://seamaf.com/storage/products/612/ZrVrSxtEGoW2mV.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
        {
          id: 6,
          img: "https://seamaf.com/storage/products/616/ojxRMmDnR7LV5K.webp",
          desc: "ring basket ball",
          price: "$2.3",
        },
        {
          id: 7,
          img: "https://seamaf.com/storage/products/614/QvOq4G5hkxvHv9.webp",
          desc: "plane white rentangle gift box (sile l )",
          price: "$2.5",
        },
        {
          id: 8,
          img: "https://seamaf.com/storage/products/613/GcRe2ZirwM8158.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
        {
          id: 9,
          img: "https://seamaf.com/storage/products/612/ZrVrSxtEGoW2mV.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
        {
          id: 10,
          img: "https://seamaf.com/storage/products/616/ojxRMmDnR7LV5K.webp",
          desc: "ring basket ball",
          price: "$2.3",
        },
        {
          id: 11,
          img: "https://seamaf.com/storage/products/614/QvOq4G5hkxvHv9.webp",
          desc: "plane white rentangle gift box (sile l )",
          price: "$2.5",
        },
        {
          id: 12,
          img: "https://seamaf.com/storage/products/613/GcRe2ZirwM8158.webp",
          desc: "Paper flags",
          price: "$2.2",
        },
      ],
    },
  ];

  const [data, setData] = useState([]);
  useEffect(() => {
    getAllProducts()
      .then((data) => {
        setData(data);
      })
      .catch((err) => console.log(err.message));
  }, []);
  return (
    <div>
      <Advisements />
      <Feature />
      <div className="px-[30px] md:px-[15px] sm:px-[40px] xl:mx-[175px] py-[20px]">
        <div className="flex items-center justify-center text-center">
          <h1 className="uppercase text-[18px] font-bold">latest product</h1>
        </div>
        <Swiper
          spaceBetween={10}
          slidesPerView={4}
          autoplay={{
            delay: 1500,
            disableOnInteraction: false,
          }}
          breakpoints={{
            320: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            480: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 30,
            },
          }}
          navigation={true}
          modules={[Autoplay, Pagination, Navigation]}
          className="mySwiper px-[15px] mt-[40px]"
        >
          {data.docs?.map((item, index) => (
            <SwiperSlide key={index}>
              {" "}
              <CardProduct
                id={item._id}
                title={item.title}
                img={item.image}
                desc={item.desc}
                price={item.price}
              />
            </SwiperSlide>
          ))}
        </Swiper>
        <div className="grid grid-cols-4 gap-4"></div>
        <div className="grid md:grid-cols-2 gap-5 my-[30px]">
          <CardSales
            img="https://seamaf.com/storage/uploads/banners/nature-4103140_1280.jpg.jpg.webp"
            title="Beading tools"
            desc="3% discount"
          />
          <CardSales
            img="https://seamaf.com/storage/uploads/banners/nature-4103140_1280.jpg.jpg.webp"
            title="Beading tools"
            desc="3% discount"
          />
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mb-[40px]">
          {data.docs?.map((item, index) => (
            <CardProduct
              id={item._id}
              title={item.title}
              key={index}
              img={item.image}
              desc={item.desc}
              price={item.price}
            />
          ))}
        </div>

        <CardSales
          img="https://seamaf.com/storage/uploads/banners/nature-4103140_1280.jpg.jpg.webp"
          title="Beading tools"
          desc="3% discount"
        />
      </div>
    </div>
  );
}
