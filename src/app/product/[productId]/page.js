"use client";
import React, { useEffect, useState, useCallback } from "react";
import { AiOutlineDoubleLeft, AiOutlineRight } from "react-icons/ai";
import Link from "next/link";
import CardProduct from "@/components/CardProduct";
import { useParams, useRouter } from "next/navigation";
import axios from "axios";
import {
  addToCart,
  getProductByCategory,
  getProductId,
} from "@/actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation, Pagination } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
const page = () => {
  const [data, setData] = useState([]);
  const [selectedValues, setSelectedValues] = useState([]);
  const [dataRelated, setDataRelated] = useState([]);
  const router = useRouter();
  const userData = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const params = useParams();

  useEffect(() => {
    getProductId(params.productId)
      .then((data) => setData(data))
      .catch((err) => console.log(err.message));
  }, []);
  const [quantity, setQuantity] = useState(1);
  const increaseQuantity = () => {
    setQuantity(quantity + 1);
    console.log(quantity);
  };
  const decreaseQuantity = () => {
    setQuantity(quantity - 1);
    if (quantity == 1) {
      setQuantity(1);
    }
    console.log(quantity);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(quantity);
    console.log(selectedValues);
    const sizeObj = selectedValues.find((obj) => obj.name === "Size");
    if (sizeObj) {
      const colorObjt = selectedValues.find((obj) => obj.name === "Màu sắc");
      if (colorObjt) {
        addToCart(
          params.productId,
          userData?.user._id,
          quantity,
          selectedValues
        ).then((data) => {
          toast.success("Thêm vào giỏ hàng thành công", {
            autoClose: 1000,  
          });
        });
      } else {
        toast.error("You need select Color", {
          autoClose: 1000,
        });
      }
    } else {
      toast.error("You need select Size", {
        autoClose: 1000,
      });
    }
  };
  const handleAttributeSet = useCallback(
    (name, value) => {
      const updatedSelectedValues = [...selectedValues];
      let isValueSelected = false;

      // Kiểm tra xem giá trị đã được chọn hay chưa
      for (let i = 0; i < updatedSelectedValues.length; i++) {
        if (updatedSelectedValues[i].name === name) {
          if (updatedSelectedValues[i].value === value) {
            // Giá trị đã được chọn, xóa nó khỏi mảng selectedValues
            updatedSelectedValues.splice(i, 1);
          } else {
            updatedSelectedValues[i].value = value;
            updatedSelectedValues[i].isSelected = true;
          }
          isValueSelected = true;
        } else {
          updatedSelectedValues[i].isSelected = true;
        }
      }

      // Nếu giá trị chưa được chọn, thêm mới vào mảng selectedValues
      if (!isValueSelected) {
        updatedSelectedValues.push({
          name: name,
          value: value,
          isSelected: true,
        });
      }

      setSelectedValues(updatedSelectedValues);
    },
    [selectedValues]
  );

  function formatCurrency(num) {
    if (num) {
      return num.toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
      });
    }
    return null;
  }
  useEffect(() => {
    getProductByCategory(data.category)
      .then((data) => setDataRelated(data))
      .catch((err) => console.log(err.message));
  },[]);
  return (
    <form
      onSubmit={handleSubmit}
      className="pt-[20px] xl:px-[15px] pb-[65px] xl:mx-[175px]"
    >
      <div>
        <Link
          href="/"
          className="text-[12px] flex items-center gap-2 px-[40px] sm:px-[30px] md:px-[30px] lg:px-0 text-[#414141]"
        >
          <AiOutlineDoubleLeft /> Back to Categories
        </Link>
        <div className="mt-[55px] mb-[30px] grid grid-cols-1 md:px-[30px] sm:grid-cols-1 md:grid-cols-2 sm:gap-2 lg:gap-8">
          {/* ảnh */}
          <div className="w-full px-[40px] sm:px-[30px] md:px-0">
            <img
              src={`http://localhost:4000/${data.image}`}
              className="w-full object-cover"
              alt=""
            />
            <div className="flex items-center gap-[30px] mt-[40px] flex-wrap">
              <div className="border-[2px] border-[#f51167]">
                <img
                  src={`http://localhost:4000/${data.image}`}
                  className="w-[116px] h-[116px] object-cover"
                  alt=""
                />
              </div>
            </div>
          </div>
          {/* Content */}
          <div className="px-[40px] sm:px-[30px] md:px-[30px] sm:mt-4">
            <h1 className="text-[18px] uppercase font-[700] md:mt-0 text-[#414141] mb-[18px]">
              {data.title}
            </h1>
            <h2 className="text-[24px] uppercase font-[700] mb-[20px]">
              {formatCurrency(data.price)}
            </h2>
            <p className="text-[12px] font-[700] mb-[10px]">
              Availability:{" "}
              <span className="text-[#f51167]">{data.category}</span>
            </p>
            {data.attribute?.map((item, index) => (
              <div className="flex flex-col sm:flex-row sm:items-start gap-2 my-[20px]">
                <h2 className="mb-[20px] sm:mb-0 float-left w-[140px] font-[700] uppercase text-[14px] tracking-wider">
                  {item.name}
                </h2>
                <div className="flex flex-col sm:flex-row sm:flex-wrap gap-2 sm:items-center">
                  {item.value.map((itemAttr, index) => (
                    <button
                      type="button"
                      onClick={() =>
                        handleAttributeSet(item.name, itemAttr.nameAtr)
                      }
                      className={`border-[1px] w-[80px] border-[#00000020] py-[5px] px-[8px] ${
                        selectedValues.find(
                          (obj) =>
                            obj.name === item.name &&
                            obj.value === itemAttr.nameAtr
                        )?.isSelected
                          ? "border-red-500"
                          : ""
                      }`}
                    >
                      {itemAttr.nameAtr}
                    </button>
                  ))}
                </div>
              </div>
            ))}

            <div className="flex items-start gap-2 mb-[40px]">
              <h2 className="float-left font-[700] w-[115px] uppercase text-[14px] tracking-wider">
                Quantity
              </h2>
              <div className="px-[15px] border border-[#333] rounded-s-sm flex items-center">
                <button
                  type="button"
                  onClick={decreaseQuantity}
                  className="w-[15px] float-left leading-[36px] text-center text-[18px] text-[#404040]"
                >
                  -
                </button>
                <input
                  type="number"
                  value={quantity}
                  className="w-[28px]  text-center float-left rounded-none outline-none text-[14px] h-[36px] bg-transparent"
                />
                <button
                  type="button"
                  onClick={increaseQuantity}
                  className="w-[15px] float-left leading-[36px] text-center text-[18px] text-[#404040]"
                >
                  +
                </button>
              </div>
            </div>
            <button
              type="submit"
              className="uppercase text-[14px] pt-[18px] pb-[14px] px-[47px] bg-[#f51147] rounded-[10px] font-[600] leading-normal inline-block text-white"
            >
              add to cart
            </button>
            <div className="mt-[40px]">
              <details class="collapse">
                <summary class="collapse-title flex justify-between border-t-[2px] border-[#00000020] text-xl font-medium">
                  <h1 className="text-[16px] font-medium uppercase">
                    DESCRIPTION
                  </h1>
                </summary>
                <div class="collapse-content">
                  <p>{data.desc}</p>
                </div>
              </details>
              <details class="collapse border-[#00000020]">
                <summary class="collapse-title flex justify-between  border-t-[2px] border-[#00000020] text-xl font-medium">
                  <h1 className="text-[16px] font-medium uppercase">
                    Shipping & returns{" "}
                  </h1>
                </summary>
                <div class="collapse-content">
                  <p>Cash on Delivery Available</p>
                </div>
              </details>
            </div>
          </div>
        </div>
        <Swiper
          spaceBetween={10}
          slidesPerView={4}
          autoplay={{
            delay: 1500,
            disableOnInteraction: false,
          }}
          breakpoints={{
            320: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            480: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 30,
            },
          }}
          navigation={true}
          modules={[Autoplay, Pagination, Navigation]}
          className="mySwiper px-[15px] mt-[40px]"
        >
          {dataRelated?.map((item, index) => (
            <SwiperSlide key={index}>
              {" "}
              <CardProduct
                id={item._id}
                title={item.title}
                key={index}
                img={item.image}
                desc={item.desc}
                price={item.price}
              />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </form>
  );
};

export default page;
