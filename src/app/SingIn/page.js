'use client'
import Link from "next/link";
import React from "react";
import {useState} from 'react'
import axios from 'axios'
import {HiMail , HiKey} from "react-icons/hi";
import { loginUser } from "@/actions/userActions";
import { useDispatch } from "react-redux";
const page = () => {
  const [infoUserLogin , setInfoUserLogin] = useState(
    {
      email:"",
      password:""
    }
  );
  const dispatch = useDispatch();
  const handleSignIn = async (event) => {
     event.preventDefault();
     dispatch(loginUser(infoUserLogin.email , infoUserLogin.password))
  }
  const handleChangeData = (event) => {
    setInfoUserLogin((infoUser) => (
      {
        ...infoUser , [event.target.name] : event.target.value
      }
     ))
  }

  return (
    <div className="xl:px-[175px] mx-auto py-[20px] w-full bg-white">
    <div className="flex flex-col md:flex-row rounded-xl border-none sm:border-[1px] overflow-hidden sm:border-[#00000020]">
    <div className="w-[45%] hidden lg:block h-full">
        <img
          src="https://seamaf.com/frontend/img/background.jpg"
          alt=""
          className="object-cover"
        />
      </div>
      {/* Form SignUp */}
      <form onSubmit={handleSignIn} className="flex-1 py-[32px] px-[48px] rounded-r-md rounded-b-md ">
        <h2 className="text-[24px] text-[#111111] font-[700] text-center mb-[20px]">
        Login to account
        </h2>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <HiMail />
          </span>
          <input
            type="text"
            name="email"
            onChange={(e) => handleChangeData(e)}
            className="flex-1  h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Email"
          />
        </div>
        <div className="w-full flex items-center py-[6px] mb-[20px] px-[15px] border-[1px] gap-3 rounded-[5px] outline-none bg-transparent  border-[#00000020]">
          <span className="text-[20px] text-[#f51167]">
            <HiKey />
          </span>
          <input
            type="password"
            onChange={(e) => handleChangeData(e)}
            name="password"
            className="flex-1 h-[30px] text-[14px] font-[400] leading-[1.5] text-[#495057] border-none outline-none"
            placeholder="Password"
          />
        </div>
        <div className="flex items-center gap-3 mb-[20px]">
            <input type="checkbox" />
            <span className="text-[14px] text-[#212529]">Remember me</span>
        </div>
        <button type="submit" className="w-full flex items-center py-[12px] justify-center mb-[16px] px-[12px] bg-[#F51167] border-[1px] gap-3 rounded-[5px] outline-none border-[#00000020]">
          <span className="text-white text-[15px] font-[700] text-center ">
           Log in
          </span>
        </button>
        <div className="flex items-end justify-end">
        <Link href='/ResetPass' className="text-[16px] text-[#007BFF] font-[700]">Forgot Password ?</Link>
        </div>
        <p className="text-center text-[14px] mb-[16px] text-[#212529]">
          or login with
        </p>
        <div className="flex-col flex sm:flex-row items-center gap-[30px] pb-[25px] border-b-[1px] border-[#00000020]">
          <button className="w-[50%] px-[12px] py-[6px] rounded-md overflow-hidden bg-[#4866a8] text-[16px] font-[700] text-white">
            FaceBook
          </button>
          <button className="flex-1 px-[12px] py-[6px] bg-[#da3f34] rounded-md overflow-hidden text-[16px] font-[700] text-white">
            Google
          </button>
        </div>
        <div className="pt-[25px] gap-2">
            <p className="text-[#212529] text-[16px] text-center">Check out as a guest ? <Link href='' className="text-[16px] font-[600] text-[#007BFF]">Click Here</Link></p>
            <p className="text-[#212529] mt-[10px] text-[16px] text-center">Don't have an account ? <Link href='/SignUp' className="text-[16px] font-[600] text-[#007BFF]">Register Here</Link></p>
        </div>
      </form>
    </div>
    </div>
  );
};

export default page;
