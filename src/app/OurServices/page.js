import React from "react";

const page = () => {
  return (
    <div className="mx-auto w-full bg-white">
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div className="lg:pl-[175px] pr-[50px] px-[30px] lg:px-0 pb-[60px] lg:pb-0 pt-[60px] bg-white flex flex-col">
          <h1 className="text-[36px] font-bold">Beading School</h1>
          <p className="text-[18px] text-[#585858] leading-[1.6] font-light">
            We help you craft a wide range of products: bags, necklaces,
            bracelets, earrings, and anything that can be beaded. In addition to
            this, we also help our students to realize the opportunity to be
            their own bosses through our beading training program using both
            pedagogy and andragogy learning methodologies to stimulate the
            interest of crafting state of the art jewels.
          </p>
          <button className="uppercase w-[180px] mt-[20px] text-[14px] border-none outline-none rounded-md leading-normal bg-[#f51167] border-[10px] px-[47px] pt-[18px] pb-[14px] text-[#fff]">
            Apply Now
          </button>
        </div>
        <div className="flex-1">
          <img
            src="https://thptcandang.edu.vn/wp-content/uploads/2023/03/hinh-nen-phong-canh-1920x1080.jpg"
            className="object-cover"
          ></img>
        </div>
      </div>
      <div className="grid grid-cols-1 xl:grid-cols-2">
        <div className="flex-1 hidden lg:block">
          <img
            src="https://thptcandang.edu.vn/wp-content/uploads/2023/03/hinh-nen-phong-canh-1920x1080.jpg"
            className="object-cover"
          ></img>
        </div>
        <div className="bg-white px-[30px] lg:px-0 flex flex-col lg:pl-[175px] pr-[50px] pt-[60px]">
          <h1 className="text-[36px] font-bold">Beading School</h1>
          <p className="text-[18px] text-[#585858] leading-[1.6] font-light">
            We help you craft a wide range of products: bags, necklaces,
            bracelets, earrings, and anything that can be beaded. In addition to
            this, we also help our students to realize the opportunity to be
            their own bosses through our beading training program using both
            pedagogy and andragogy learning methodologies to stimulate the
            interest of crafting state of the art jewels.
          </p>
          <button className="uppercase w-[180px] mt-[20px] text-[14px] border-none outline-none rounded-md leading-normal bg-[#f51167] border-[10px] px-[47px] pt-[18px] pb-[14px] text-[#fff]">
            Apply Now
          </button>
        </div>
        <div className="flex-1 block mt-[60px] lg:mt-0 lg:hidden">
          <img
            src="https://thptcandang.edu.vn/wp-content/uploads/2023/03/hinh-nen-phong-canh-1920x1080.jpg"
            className="object-cover"
          ></img>
        </div>
      </div>
      <div className="grid grid-cols-1 xl:grid-cols-2">
        <div className="bg-white flex flex-col px-[30px] pb-[60px] lg:pb-0 lg:px-0 pt-[60px] lg:pl-[175px] pr-[50px]">
          <h1 className="text-[36px] font-bold">Beading School</h1>
          <p className="text-[18px] text-[#585858] leading-[1.6] font-light">
            We help you craft a wide range of products: bags, necklaces,
            bracelets, earrings, and anything that can be beaded. In addition to
            this, we also help our students to realize the opportunity to be
            their own bosses through our beading training program using both
            pedagogy and andragogy learning methodologies to stimulate the
            interest of crafting state of the art jewels.
          </p>
          <button className="uppercase w-[180px] mt-[20px] text-[14px] border-none outline-none rounded-md leading-normal bg-[#f51167] border-[10px] px-[47px] pt-[18px] pb-[14px] text-[#fff]">
            Apply Now
          </button>
        </div>
        <div className="flex-1">
          <img
            src="https://thptcandang.edu.vn/wp-content/uploads/2023/03/hinh-nen-phong-canh-1920x1080.jpg"
            className="object-cover"
          ></img>
        </div>
      </div>
    </div>
  );
};

export default page;
