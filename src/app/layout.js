"use client";
import "./globals.css";
import { Josefin_Sans } from "next/font/google";
const Jose = Josefin_Sans({ subsets: ["latin"] });
import Header from "@/components/Header";
import Footer from "@/components/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import myStore from "../reducers/configureStore";
export default function RootLayout({ children }) {
  const { store, persistor } = myStore;
  return (
    <Provider store={store}>
      <html lang="en">
        <body className={Jose.className}>
          <PersistGate loading={null} persistor={persistor}>
              <ToastContainer />
              <Header />
              {children}
              <Footer />
          </PersistGate>
        </body>
      </html>
    </Provider>
  );
}
